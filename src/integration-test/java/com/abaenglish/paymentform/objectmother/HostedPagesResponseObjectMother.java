package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.controller.v1.dto.hostedpage.HostedPageResponse;

import java.util.ArrayList;
import java.util.List;

public class HostedPagesResponseObjectMother {

    public static List<HostedPageResponse> defaultHostedPagesResponse() {
        //Final response
        HostedPageResponse hostedPagesResponse1 = HostedPageResponse.Builder.aHostedPagesResponse()
                .pageId("2c92c0f9558201b00155924bb7392058")
                .methodName("CreditCard")
                .gatewayName("Adyen Test Gateway")
                .build();
        HostedPageResponse hostedPagesResponse2 = HostedPageResponse.Builder.aHostedPagesResponse()
                .pageId("2c92c0f95595724b0155960773197a73")
                .methodName("BankTransfer")
                .gatewayName("Adyen Test Gateway")
                .build();

        List<HostedPageResponse> hostedPagesResponses = new ArrayList<>();
        hostedPagesResponses.add(hostedPagesResponse1);
        hostedPagesResponses.add(hostedPagesResponse2);

        return hostedPagesResponses;
    }
}
