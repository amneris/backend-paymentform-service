package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.domain.PaymentGateway;

public class PaymentGatewayObjectMother {

    public static PaymentGateway paymentGatewayAdyen() {
        PaymentGateway paymentGateway = PaymentGateway.Builder.aPaymentGateway()
                .id(1L)
                .name("Adyen Test Gateway")
                .gateway("Adyen")
                .active(true)
                .build();

        return paymentGateway;
    }

}
