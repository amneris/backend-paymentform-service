package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.domain.Country;

public class CountryObjectMother {

    public final static Country countryCHN() {

        Country country = Country.Builder.aCountry()
                .id(1L)
                .isocode2("CN")
                .isocode3("CHN")
                .isom49(156)
                .nameEnglish("China")
                .nameLocal("Zhong Guo")
                .build();
        return country;
    }

    public final static Country countryLKA() {
        Country country = Country.Builder.aCountry()
                .id(2L)
                .isocode2("LK")
                .isocode3("LKA")
                .isom49(144)
                .nameEnglish("Sri Lanka")
                .nameLocal("Sri Lanka")
                .build();
        return country;
    }
}
