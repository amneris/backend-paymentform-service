package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.domain.PaymentMethod;

public class PaymentMethodObjectMother {

    public static PaymentMethod paymentMethodCC() {
        PaymentMethod paymentMethod = PaymentMethod.Builder.aPaymentMethod()
                .id(1L)
                .name("Credit_Card")
                .active(true)
                .build();
        return paymentMethod;
    }

}
