package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.domain.PaymentHostedPages;

public class PaymentHostedPagesObjectMother {

    public static PaymentHostedPages paymentHostedPagesCCAdyen() {

        PaymentHostedPages paymentHostedPages = PaymentHostedPages.Builder.aPaymentHostedPages()
                .id(5L)
                .pageId("2c92c0f9558201b00155924bb7392058")
                .pageName("LOCAL - Adyen")
                .pageType("Credit Card")
                .pageVersion(2)
                .active(true)
                .build();

        return paymentHostedPages;
    }

}
