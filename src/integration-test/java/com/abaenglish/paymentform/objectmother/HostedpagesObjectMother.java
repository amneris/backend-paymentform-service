package com.abaenglish.paymentform.objectmother;

import com.abaenglish.external.zuora.rest.domain.hostedpage.Hostedpage;

import java.util.ArrayList;
import java.util.List;

public class HostedpagesObjectMother {

    public static List<Hostedpage> hostedpages() {

        List<Hostedpage> hostedpages = new ArrayList<>();

        Hostedpage hostedpage1 = Hostedpage.Builder.aHostedpage()
                .pageId("2c92c0f854a35e960154a965550b6ece")
                .pageName("Credit Card HPM 2.0")
                .pageType("Credit Card")
                .pageVersion(2)
                .build();
        hostedpages.add(hostedpage1);

        Hostedpage hostedpage2 = Hostedpage.Builder.aHostedpage()
                .pageId("2c92c0f854a35e960154a9668350793a")
                .pageName("Zilla HPM 2.0 - SEPA")
                .pageType("SEPA")
                .pageVersion(2)
                .build();
        hostedpages.add(hostedpage2);

        return hostedpages;
    }
}
