package com.abaenglish.paymentform.objectmother;

import com.abaenglish.paymentform.domain.PaymentForm;

import java.util.ArrayList;
import java.util.List;

public class PaymentFormObjectMother {

    public static List<PaymentForm> defaultPaymentForm() {

        PaymentForm paymentForm = PaymentForm.Builder.aPaymentForm()
                .id(1L)
                .countryIso3("MEX")
                .paymentMethod(PaymentMethodObjectMother.paymentMethodCC())
                .paymentGateway(PaymentGatewayObjectMother.paymentGatewayAdyen())
                .paymentHostedPages(PaymentHostedPagesObjectMother.paymentHostedPagesCCAdyen())
                .environmentUrl("localhost")
                .active(true)
                .build();

        List<PaymentForm> paymentForms = new ArrayList<>();
        paymentForms.add(paymentForm);

        return paymentForms;

    }

}
