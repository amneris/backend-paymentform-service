package com.abaenglish.paymentform.objectmother;

import com.abaenglish.external.abawebapps.domain.Currency;
import com.abaenglish.user.controller.v1.dto.response.CurrencyApiResponse;

public class CurrencyObjectMother {

    public final static CurrencyApiResponse currencyUSD() {
        CurrencyApiResponse currency = CurrencyApiResponse.Builder.aCurrencyApiResponse()
                .abaIdCurrency("USD")
                .build();
        return currency;
    }

    public final static Currency currencyUSDExternal() {
        Currency currency = Currency.Builder.aCurrency()
                .abaIdCurrency("USD")
                .build();
        return currency;
    }

    public final static CurrencyApiResponse currencyEUR() {
        CurrencyApiResponse currency = CurrencyApiResponse.Builder.aCurrencyApiResponse()
                .abaIdCurrency("EUR")
                .build();
        return currency;
    }

    public final static CurrencyApiResponse currencyMXN() {
        CurrencyApiResponse currency = CurrencyApiResponse.Builder.aCurrencyApiResponse()
                .abaIdCurrency("MXN")
                .build();
        return currency;
    }
}
