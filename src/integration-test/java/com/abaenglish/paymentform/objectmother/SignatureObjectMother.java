package com.abaenglish.paymentform.objectmother;

import com.abaenglish.external.zuora.rest.domain.signature.Signature;

public class SignatureObjectMother {

    public static Signature defaultSignature() {
        Signature signature = Signature.Builder.aSignature()
                .witMethod("POST")
                .witUri("https://apisandbox.zuora.com/apps/PublicHostedPageLite.do")
                .witPageId("2c92c0f854a35e960154a9668350793a")
                .build();

        return signature;

    }

}
