package com.abaenglish.paymentform.objectmother;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;

public class ResponseSignatureObjectMother {

    public static ResponseSignature defaultResponseSignature() {
        ResponseSignature responseSignature = ResponseSignature.Builder.aResponseSignature()
                .signature("WOvRRPys87IuHK8GahMIVDpP3ddEsLolEzjAeZf9qXTYf1/se/gkTtY8M6gwHScF/cBv0xc4cG7iKt7skS8T8nvkmU1CcUY8LvRTwragxGPLzjFLxIZhDkEmxSKoJL1yOaOPeB+Tl9X4eYrL9OiLGGy0inV59GQ/84NChK4y+Iox32oXg2mFe85SmjiJxYnZRMXJvDM9AFo/taOhxvupqYaY7i2dMPwrUTKqAXuJ9LM3QrmcJoQ1Uf5Wv3CIy3NKjQ6gQFvPN0WV8LOHGgm2U/PEdpZpsEjLGkPOToW6fJ4VQMnblXmmeyl5Qlkje8c93wDLTlv3KrUrrn0N9ZMJ+g==")
                .token("Nh1I0ms61giC8zAHzhf2r0AvMdiTFkFL")
                .tenantId("14418")
                .key("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnlvMywPSWwCB7yHJ2Z4V+sc+7VC/+XBkMOWpPKWa4oShZLr+FGzznazYM6PTxUJLYTsnlmBOzOg1Wsv7cDRgGtZvzHZ5pvPV23b0wjdc/AjURS5m4tZ6Fa1D6KdY4nI6qsGN5xsIH06Ketb89nQrkQMBEtDMIljxjpk/mq5yqysNkK3j2skJWDOFNRDZxPaa+W/9xvp4Z19iwmasC90xF+pRuZ9lvkW31rcMHj/JcWHNjBDQ3Hq1mdNKlog5Z2WYlgd0SZ6E6MsSGHU5594PSiBoLQtOMsIEshEED8nIZqxDPqx2AmJd3BJw/+xrGWbD4P7OI7UwnsUT5I27w22xqQIDAQAB")
                .success(true)
                .build();

        return responseSignature;

    }

}
