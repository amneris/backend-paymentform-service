package com.abaenglish.paymentform.controller.v1;

import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.external.zuora.rest.service.impl.ZuoraExternalServiceRest;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

public class TestConfiguration {

    @Bean
    @Primary
    public IZuoraExternalServiceRest zuoraExternalService() {
        return Mockito.mock(ZuoraExternalServiceRest.class);
    }

    @Bean
    @Primary
    public IUserServiceFeign userServiceFeign() {
        return Mockito.mock(IUserServiceFeign.class);
    }

}
