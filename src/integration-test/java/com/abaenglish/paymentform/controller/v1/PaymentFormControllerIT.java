package com.abaenglish.paymentform.controller.v1;

import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.paymentform.PaymentFormApplication;
import com.abaenglish.paymentform.controller.v1.dto.hostedpage.HostedPageResponse;
import com.abaenglish.paymentform.objectmother.HostedPagesResponseObjectMother;
import com.abaenglish.user.controller.v1.dto.response.CountryIsoApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.specification.ResponseSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PaymentFormApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
public class PaymentFormControllerIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    @DatabaseSetup(value = "hostedpages.xml")
    @DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "hostedpages-empty.xml")
    public void getHostedPages() throws Exception {

        List<HostedPageResponse> hostedPagesResponse = HostedPagesResponseObjectMother.defaultHostedPagesResponse();

        CountryIsoApiResponse countryIso = CountryIsoApiResponse.Builder.aCountryIsoApiResponse()
                .abaCountryIso("MEX")
                .build();

        Mockito.when(userServiceFeign.getCountry(anyLong())).thenReturn(countryIso);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();
        builder.expectStatusCode(200);
        for (int i = 0; i < 2; i++) {

            builder.expectBody("pageId[" + i + "]", is(hostedPagesResponse.get(i).getPageId()));
            builder.expectBody("methodName[" + i + "]", is(hostedPagesResponse.get(i).getMethodName()));
            builder.expectBody("gatewayName[" + i + "]", is(hostedPagesResponse.get(i).getGatewayName()));

        }

        ResponseSpecification responseSpec = builder.build();

        //Header header = new Header("Origin", "http://payments-dev.aba.land");
        Header header = new Header("Origin", "http://localhost:3000");

        given().
                header(header).
                contentType(ContentType.JSON).
                when().
                get("/api/v1/paymentforms?user=265").
                then().
                spec(responseSpec);
    }

    @Test
    public void getHostedPagesErrorCountry() {

        List<HostedPageResponse> hostedPagesResponse = HostedPagesResponseObjectMother.defaultHostedPagesResponse();
        doThrow(BadRequestApiException.class).when(userServiceFeign).getCountry(265L);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(404);
        builder.expectBody("status", is(404));
        builder.expectBody("message", is("Country not found"));
        builder.expectBody("abaCode", is("SUB0012"));

        ResponseSpecification responseSpec = builder.build();

        Header header = new Header("Origin", "http://localhost:3000");

        given().
                header(header).
                contentType(ContentType.JSON).
                when().
                get("/api/v1/paymentforms?user=265").
                then().
                spec(responseSpec);

    }
}
