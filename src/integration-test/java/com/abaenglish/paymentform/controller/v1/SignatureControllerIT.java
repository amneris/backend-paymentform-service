package com.abaenglish.paymentform.controller.v1;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;
import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.paymentform.PaymentFormApplication;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.abaenglish.paymentform.objectmother.ResponseSignatureObjectMother.defaultResponseSignature;
import static com.abaenglish.paymentform.objectmother.SignatureObjectMother.defaultSignature;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PaymentFormApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
public class SignatureControllerIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceRest zuoraExternalService;

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    public void getSignatureForAPageId() throws Exception {

        Signature signature = defaultSignature();
        ResponseSignature responseSignature = defaultResponseSignature();

        Mockito.when(zuoraExternalService.getSignature(Matchers.any(Signature.class))).thenReturn(responseSignature);

        given().
                contentType(ContentType.JSON).
                body(signature).
                when().
                get("/api/v1/signatures?page=" + signature).
                then().
                statusCode(200).
                body("success", is(true));
    }

}
