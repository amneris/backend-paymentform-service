package com.abaenglish.paymentform.repository;

import com.abaenglish.paymentform.PaymentFormApplication;
import com.abaenglish.paymentform.controller.v1.TestConfiguration;
import com.abaenglish.paymentform.domain.PaymentForm;
import com.abaenglish.paymentform.domain.QPaymentForm;
import com.abaenglish.paymentform.repository.predicate.PaymentFormPredicate;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PaymentFormApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})

@DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "../controller/v1/hostedpages.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../controller/v1/hostedpages-empty.xml")
public class PaymentFormRepositoryIT {

    protected static final Long PAYMENT_GATEWAY_ID = 1L;
    protected static final String PAYMENT_GATEWAY_COUNTRYISO_3 = "MEX";
    protected static final Long PAYMENT_GATEWAY_METHOD_ID = 4L;
    protected static final Long PAYMENT_GATEWAY_GATEWAY_ID = 6L;
    protected static final Long PAYMENT_GATEWAY_HOSTEDPAGES_ID = 1L;
    protected static final String PAYMENT_GATEWAY_ENVIRONMENTURL = "payments-dev.aba.land";

    @Autowired
    private PaymentFormRepository paymentFormRepository;

    @Test
    public void findOne() {
        Optional<PaymentForm> paymentForm = paymentFormRepository.findOne(PAYMENT_GATEWAY_ID);

        assertEquals(true, paymentForm.isPresent());
        assertEquals(PAYMENT_GATEWAY_ID, paymentForm.get().getId());
        assertEquals(PAYMENT_GATEWAY_COUNTRYISO_3, paymentForm.get().getCountryIso3());
        assertEquals(PAYMENT_GATEWAY_METHOD_ID, paymentForm.get().getPaymentMethod().getId());
        assertEquals(PAYMENT_GATEWAY_GATEWAY_ID, paymentForm.get().getPaymentGateway().getId());
        assertEquals(PAYMENT_GATEWAY_HOSTEDPAGES_ID, paymentForm.get().getPaymentHostedPages().getId());
        assertEquals(PAYMENT_GATEWAY_ENVIRONMENTURL, paymentForm.get().getEnvironmentUrl());

    }

    @Test
    public void findfindByCountryId() {

        List<PaymentForm> paymentForms = new ArrayList<>();
        paymentFormRepository.findAll(PaymentFormPredicate.findByCountryId_Environment(PAYMENT_GATEWAY_COUNTRYISO_3, PAYMENT_GATEWAY_ENVIRONMENTURL), QPaymentForm.paymentForm.paymentOrder.asc()).forEach(paymentForms::add);

        assertEquals(3, paymentForms.size());

        assertEquals(PAYMENT_GATEWAY_ID, paymentForms.get(0).getId());
        assertEquals(PAYMENT_GATEWAY_COUNTRYISO_3, paymentForms.get(0).getCountryIso3());
        assertEquals(PAYMENT_GATEWAY_METHOD_ID, paymentForms.get(0).getPaymentMethod().getId());
        assertEquals(PAYMENT_GATEWAY_GATEWAY_ID, paymentForms.get(0).getPaymentGateway().getId());
        assertEquals(PAYMENT_GATEWAY_HOSTEDPAGES_ID, paymentForms.get(0).getPaymentHostedPages().getId());
        assertEquals(PAYMENT_GATEWAY_ENVIRONMENTURL, paymentForms.get(0).getEnvironmentUrl());

    }

    @Test
    public void ListAll() {
        List<PaymentForm> paymentForms = paymentFormRepository.findAll();

        assertEquals(18, paymentForms.size());

    }

    @Test
    public void findOneFail() {
        Optional<PaymentForm> paymentForm = paymentFormRepository.findOne(-1L);

        assertThat(paymentForm.isPresent()).isEqualTo(false);

    }

    @Test
    public void findfindByCountryIdFais() {

        List<PaymentForm> paymentForms = new ArrayList<>();
        paymentFormRepository.findAll(PaymentFormPredicate.findByCountryId_Environment("XXX", "nolose.com"), QPaymentForm.paymentForm.paymentOrder.asc()).forEach(paymentForms::add);

        assertThat(paymentForms.size()).isEqualTo(0);

    }

}
