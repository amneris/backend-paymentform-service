package com.abaenglish.paymentform.repository;

import com.abaenglish.paymentform.PaymentFormApplication;
import com.abaenglish.paymentform.controller.v1.TestConfiguration;
import com.abaenglish.paymentform.domain.Country;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PaymentFormApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
@DatabaseSetup(value = "country.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "country-empty.xml")
public class CountryRepositoryIT {

    protected static final String COUNTRY_ISOCODE_3 = "ABA";
    protected static final Long COUNTRY_ID = 1L;

    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findByIso3() {

        Optional<Country> country = countryRepository.findByIsocode3IgnoreCase(COUNTRY_ISOCODE_3);

        assertThat(country.isPresent()).isEqualTo(true);

        assertEquals(COUNTRY_ISOCODE_3, country.get().getIsocode3());
        assertEquals(COUNTRY_ID, country.get().getId());
    }

    @Test
    public void findByIso3Fail() {

        Optional<Country> country = countryRepository.findByIsocode3IgnoreCase("ZZZ");

        assertThat(country.isPresent()).isEqualTo(false);

    }
}
