package com.abaenglish.paymentform.domain;

import com.abaenglish.paymentform.objectmother.CountryObjectMother;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryTest {

    protected static final Long COUNTRY_1_ID = 1L;
    protected static final String COUNTRY_1_name_english = "China";
    protected static final String COUNTRY_1_name_local = "Zhong Guo";
    protected static final String COUNTRY_1_isocode2 = "CN";
    protected static final String COUNTRY_1_isocode3 = "CHN";
    protected static final Integer COUNTRY_1_isom49 = 156;

    protected static final Long COUNTRY_4_ID = 2L;
    protected static final String COUNTRY_4_name_english = "Sri Lanka";
    protected static final String COUNTRY_4_name_local = "Sri Lanka";
    protected static final String COUNTRY_4_isocode2 = "LK";
    protected static final String COUNTRY_4_isocode3 = "LKA";
    protected static final Integer COUNTRY_4_isom49 = 144;

    @Test
    public void equals() {
        Country china = CountryObjectMother.countryCHN();

        assertThat(china.getId()).isEqualTo(COUNTRY_1_ID);
        assertThat(china.getNameEnglish()).isEqualTo(COUNTRY_1_name_english);
        assertThat(china.getNameLocal()).isEqualTo(COUNTRY_1_name_local);
        assertThat(china.getIsocode2()).isEqualTo(COUNTRY_1_isocode2);
        assertThat(china.getIsocode3()).isEqualTo(COUNTRY_1_isocode3);
        assertThat(china.getIsom49()).isEqualTo(COUNTRY_1_isom49);

        Country itemClone = CountryObjectMother.countryCHN();
        assertThat(itemClone.getId()).isEqualTo(COUNTRY_1_ID);
        assertThat(itemClone.getNameEnglish()).isEqualTo(COUNTRY_1_name_english);
        assertThat(itemClone.getNameLocal()).isEqualTo(COUNTRY_1_name_local);
        assertThat(itemClone.getIsocode2()).isEqualTo(COUNTRY_1_isocode2);
        assertThat(itemClone.getIsocode3()).isEqualTo(COUNTRY_1_isocode3);
        assertThat(itemClone.getIsom49()).isEqualTo(COUNTRY_1_isom49);

        Country itemOther = CountryObjectMother.countryLKA();
        assertThat(itemOther.getId()).isEqualTo(COUNTRY_4_ID);
        assertThat(itemOther.getNameEnglish()).isEqualTo(COUNTRY_4_name_english);
        assertThat(itemOther.getNameLocal()).isEqualTo(COUNTRY_4_name_local);
        assertThat(itemOther.getIsocode2()).isEqualTo(COUNTRY_4_isocode2);
        assertThat(itemOther.getIsocode3()).isEqualTo(COUNTRY_4_isocode3);
        assertThat(itemOther.getIsom49()).isEqualTo(COUNTRY_4_isom49);

        assertTrue(china.equals(itemClone));
        assertFalse(china.equals(itemOther));
        assertFalse(itemClone.equals(itemOther));
    }


    @Test
    public void testHashCode() {
        Country item = CountryObjectMother.countryCHN();
        Country itemClone = CountryObjectMother.countryCHN();
        Country itemOther = CountryObjectMother.countryLKA();

        assertEquals(item.hashCode(),itemClone.hashCode());
        assertNotEquals(item.hashCode(),itemOther.hashCode());
        assertNotEquals(itemClone.hashCode(),itemOther.hashCode());
    }

}