package com.abaenglish.paymentform.domain;

import com.abaenglish.paymentform.objectmother.PaymentFormObjectMother;
import com.abaenglish.paymentform.objectmother.PaymentGatewayObjectMother;
import com.abaenglish.paymentform.objectmother.PaymentHostedPagesObjectMother;
import com.abaenglish.paymentform.objectmother.PaymentMethodObjectMother;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PaymentFormTest {

    @Test
    public void equals() {

        List<PaymentForm> paymentForm = PaymentFormObjectMother.defaultPaymentForm();

        assertThat(paymentForm.get(0).getId()).isEqualTo(1L);
        assertThat(paymentForm.get(0).getActive()).isEqualTo(true);
        assertThat(paymentForm.get(0).getCountryIso3()).isEqualTo("MEX");
        assertThat(paymentForm.get(0).getPaymentMethod()).isEqualTo(PaymentMethodObjectMother.paymentMethodCC());
        assertThat(paymentForm.get(0).getPaymentGateway()).isEqualTo(PaymentGatewayObjectMother.paymentGatewayAdyen());
        assertThat(paymentForm.get(0).getPaymentHostedPages()).isEqualTo(PaymentHostedPagesObjectMother.paymentHostedPagesCCAdyen());
        assertThat(paymentForm.get(0).getEnvironmentUrl()).isEqualTo("localhost");

        List<PaymentForm> paymentClone = PaymentFormObjectMother.defaultPaymentForm();

        assertThat(paymentClone.get(0).getId()).isEqualTo(1L);
        assertThat(paymentClone.get(0).getActive()).isEqualTo(true);
        assertThat(paymentClone.get(0).getCountryIso3()).isEqualTo("MEX");
        assertThat(paymentClone.get(0).getPaymentMethod()).isEqualTo(PaymentMethodObjectMother.paymentMethodCC());
        assertThat(paymentClone.get(0).getPaymentGateway()).isEqualTo(PaymentGatewayObjectMother.paymentGatewayAdyen());
        assertThat(paymentClone.get(0).getPaymentHostedPages()).isEqualTo(PaymentHostedPagesObjectMother.paymentHostedPagesCCAdyen());
        assertThat(paymentClone.get(0).getEnvironmentUrl()).isEqualTo("localhost");

    }

    @Test
    public void testHashCode() {
        List<PaymentForm> item = PaymentFormObjectMother.defaultPaymentForm();
        List<PaymentForm> itemClone = PaymentFormObjectMother.defaultPaymentForm();

        assertEquals(item.hashCode(), itemClone.hashCode());
    }

}