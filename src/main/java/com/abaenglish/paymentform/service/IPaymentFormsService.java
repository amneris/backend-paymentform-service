package com.abaenglish.paymentform.service;

import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.paymentform.domain.PaymentForm;

import java.util.List;

public interface IPaymentFormsService {

    List<PaymentForm> getHostedPages(Long userId, String environment) throws NotFoundApiException;
}
