package com.abaenglish.paymentform.service;

import com.abaenglish.paymentform.domain.Country;

import java.util.Optional;

public interface ICountryService {
    Optional<Country> findByIso3(String iso3);
}
