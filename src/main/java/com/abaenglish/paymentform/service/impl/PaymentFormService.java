package com.abaenglish.paymentform.service.impl;

import com.abaenglish.paymentform.domain.PaymentForm;
import com.abaenglish.paymentform.domain.QPaymentForm;
import com.abaenglish.paymentform.repository.PaymentFormRepository;
import com.abaenglish.paymentform.repository.predicate.PaymentFormPredicate;
import com.abaenglish.paymentform.service.IPaymentFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentFormService implements IPaymentFormService {

    @Autowired
    private PaymentFormRepository paymentFormRepository;

    public Optional<PaymentForm> findOne(Long id) {
        return paymentFormRepository.findOne(id);
    }

    public List<PaymentForm> listAll() {
        return paymentFormRepository.findAll();
    }

    public List<PaymentForm> findByCountryIso3(String countryIso3, String environment) {
        List<PaymentForm> target = new ArrayList<>();
        paymentFormRepository.findAll(PaymentFormPredicate.findByCountryId_Environment(countryIso3, environment), QPaymentForm.paymentForm.paymentOrder.asc()).forEach(target::add);
        return target;
    }
}