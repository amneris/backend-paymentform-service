package com.abaenglish.paymentform.service.impl;

import com.abaenglish.paymentform.domain.Country;
import com.abaenglish.paymentform.repository.CountryRepository;
import com.abaenglish.paymentform.service.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CountryService implements ICountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public Optional<Country> findByIso3(String iso3) {
        return countryRepository.findByIsocode3IgnoreCase(iso3);
    }

}
