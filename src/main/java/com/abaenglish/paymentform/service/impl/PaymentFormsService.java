package com.abaenglish.paymentform.service.impl;

import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.paymentform.domain.Country;
import com.abaenglish.paymentform.domain.PaymentForm;
import com.abaenglish.paymentform.exception.ErrorMessages;
import com.abaenglish.paymentform.properties.SubscriptionProperties;
import com.abaenglish.paymentform.service.ICountryService;
import com.abaenglish.paymentform.service.IPaymentFormsService;
import com.abaenglish.paymentform.service.IPaymentFormService;
import com.abaenglish.user.controller.v1.dto.response.CountryIsoApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentFormsService implements IPaymentFormsService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentFormsService.class);

    @Autowired
    private ICountryService countryService;

    @Autowired
    private IPaymentFormService paymentFormService;

    @Autowired
    private SubscriptionProperties paymentProperties;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Override
    public List<PaymentForm> getHostedPages(Long userId, String environment) throws NotFoundApiException {

        CountryIsoApiResponse countryIso = null;
        try {
            countryIso = userServiceFeign.getCountry(userId);
        } catch (Exception e) {
            logger.error("Problem with Feign Client User: " + e);
            throw new NotFoundApiException(ErrorMessages.COUNTRY_NOT_FOUND.getError());
        }

        Optional<Country> country = countryService.findByIso3(countryIso.getAbaCountryIso());

        List<PaymentForm> paymentForms = new ArrayList<>();

        if (country.isPresent()) {
            paymentForms = paymentFormService.findByCountryIso3(country.get().getIsocode3(), environment);
        }

        if (paymentForms.isEmpty()) {
            paymentForms = paymentFormService.findByCountryIso3(paymentProperties.getDefaultcountryiso3(), environment);
        }

        if (paymentForms.isEmpty()) {
            throw new NotFoundApiException(ErrorMessages.ENVIRONMENT_NOT_FOUND.getError());
        }

        return paymentForms;
    }
}
