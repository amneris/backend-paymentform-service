package com.abaenglish.paymentform.service.impl;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.external.zuora.rest.domain.signature.Signature;
import com.abaenglish.external.zuora.rest.properties.ZuoraRestProperties;
import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.paymentform.service.ISignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignatureService implements ISignatureService {

    @Autowired
    private IZuoraExternalServiceRest zuoraExternalService;

    @Autowired
    private ZuoraRestProperties zuoraProperties;

    @Override
    public ResponseSignature getSignature(String page) {

        Signature signature = Signature.Builder.aSignature()
                .witMethod("POST")
                .witUri(zuoraProperties.getHostSignature() + "/apps/PublicHostedPageLite.do")
                .witPageId(page)
                .build();

        return zuoraExternalService.getSignature(signature);
    }
}
