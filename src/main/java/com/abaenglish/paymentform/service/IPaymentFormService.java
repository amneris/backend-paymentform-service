package com.abaenglish.paymentform.service;

import com.abaenglish.paymentform.domain.PaymentForm;

import java.util.List;
import java.util.Optional;

public interface IPaymentFormService {

    Optional<PaymentForm> findOne(Long id);

    List<PaymentForm> listAll();

    List<PaymentForm> findByCountryIso3(String countryIso3, String environment);
}
