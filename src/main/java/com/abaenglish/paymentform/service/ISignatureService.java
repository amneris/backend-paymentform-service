package com.abaenglish.paymentform.service;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;

public interface ISignatureService {

    ResponseSignature getSignature(String page);
}
