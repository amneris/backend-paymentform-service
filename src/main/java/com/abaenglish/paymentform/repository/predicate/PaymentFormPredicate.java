package com.abaenglish.paymentform.repository.predicate;

import com.abaenglish.paymentform.domain.QPaymentForm;
import com.querydsl.core.types.Predicate;

public class PaymentFormPredicate {

    private PaymentFormPredicate() {
        throw new IllegalAccessError("Utility class");
    }

    public static Predicate findByCountryId_Environment(String countryIso3, String environment) {

        Predicate predicate = QPaymentForm.paymentForm.countryIso3.eq(countryIso3)
                .and(QPaymentForm.paymentForm.environmentUrl.eq(environment))
                .and(QPaymentForm.paymentForm.paymentHostedPages.active.eq(true))
                .and(QPaymentForm.paymentForm.paymentMethod.active.eq(true))
                .and(QPaymentForm.paymentForm.paymentGateway.active.eq(true))
                .and(QPaymentForm.paymentForm.active.eq(true))
                .and(QPaymentForm.paymentForm.paymentProvider.active.eq(true));

        return predicate;
    }

}
