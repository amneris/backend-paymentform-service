package com.abaenglish.paymentform.repository;

import com.abaenglish.boot.data.repository.BaseRepository;
import com.abaenglish.paymentform.domain.Country;

import java.util.Optional;

public interface CountryRepository extends BaseRepository<Country, Long> {
    Optional<Country> findByIsocode3IgnoreCase(String isocode3);
}