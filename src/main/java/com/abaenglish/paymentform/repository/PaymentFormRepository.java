package com.abaenglish.paymentform.repository;

import com.abaenglish.boot.data.repository.BaseRepository;
import com.abaenglish.paymentform.domain.PaymentForm;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface PaymentFormRepository extends BaseRepository<PaymentForm, Long>, QueryDslPredicateExecutor<PaymentForm> {
}