package com.abaenglish.paymentform.domain;

import com.abaenglish.boot.domain.DomainObject;

import javax.persistence.*;

@Entity
@Table(name = "payment_hostedpages")
public class PaymentHostedPages extends DomainObject {

    private static final long serialVersionUID = 2447491256524657839L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "pageid", insertable = true, updatable = true, nullable = false, length = 100)
    private String pageId;
    @Column(name = "pagename", insertable = true, updatable = true, nullable = false, length = 100)
    private String pageName;
    @Column(name = "pagetype", insertable = true, updatable = true, nullable = false, length = 100)
    private String pageType;
    @Column(name = "pageversion", insertable = true, updatable = true, nullable = false)
    private Integer pageVersion;
    @Column(name = "active", nullable = false)
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public Integer getPageVersion() {
        return pageVersion;
    }

    public void setPageVersion(Integer pageVersion) {
        this.pageVersion = pageVersion;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PaymentHostedPages))
            return false;

        PaymentHostedPages that = (PaymentHostedPages) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static final class Builder {
        private Long id;
        private String pageId;
        private String pageName;
        private String pageType;
        private Integer pageVersion;
        private Boolean active;

        private Builder() {
        }

        public static Builder aPaymentHostedPages() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder pageId(String pageId) {
            this.pageId = pageId;
            return this;
        }

        public Builder pageName(String pageName) {
            this.pageName = pageName;
            return this;
        }

        public Builder pageType(String pageType) {
            this.pageType = pageType;
            return this;
        }

        public Builder pageVersion(Integer pageVersion) {
            this.pageVersion = pageVersion;
            return this;
        }

        public Builder active(Boolean active) {
            this.active = active;
            return this;
        }

        public PaymentHostedPages build() {
            PaymentHostedPages paymentHostedPages = new PaymentHostedPages();
            paymentHostedPages.setId(id);
            paymentHostedPages.setPageId(pageId);
            paymentHostedPages.setPageName(pageName);
            paymentHostedPages.setPageType(pageType);
            paymentHostedPages.setPageVersion(pageVersion);
            paymentHostedPages.setActive(active);
            return paymentHostedPages;
        }
    }

}