package com.abaenglish.paymentform.domain;

import com.abaenglish.boot.domain.DomainObject;

import javax.persistence.*;

@Entity
@Table(name = "payment_gateway")
public class PaymentGateway extends DomainObject {

    private static final long serialVersionUID = -8920340741602129889L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name", insertable = true, updatable = true, nullable = false, length = 100)
    private String name;
    @Column(name = "gateway", insertable = true, updatable = true, nullable = false, length = 100)
    private String gateway;
    @Column(name = "active", nullable = false)
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PaymentGateway))
            return false;

        PaymentGateway that = (PaymentGateway) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String gateway;
        private Boolean active;

        private Builder() {
        }

        public static Builder aPaymentGateway() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder gateway(String gateway) {
            this.gateway = gateway;
            return this;
        }

        public Builder active(Boolean active) {
            this.active = active;
            return this;
        }

        public PaymentGateway build() {
            PaymentGateway paymentGateway = new PaymentGateway();
            paymentGateway.setId(id);
            paymentGateway.setName(name);
            paymentGateway.setGateway(gateway);
            paymentGateway.setActive(active);
            return paymentGateway;
        }
    }

}