package com.abaenglish.paymentform.domain;

import com.abaenglish.boot.domain.DomainObject;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class Country extends DomainObject {

    private static final long serialVersionUID = 8397837743463390960L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name_english", insertable = true, updatable = true, nullable = false, length = 100)
    private String nameEnglish;
    @Column(name = "name_local", insertable = true, updatable = true, nullable = true, length = 100)
    private String nameLocal;
    @Column(name = "isocode2", insertable = true, updatable = true, nullable = false, length = 2)
    private String isocode2;
    @Column(name = "isocode3", insertable = true, updatable = true, nullable = false, length = 3)
    private String isocode3;
    @Column(name = "isom49", insertable = true, updatable = true, nullable = false)
    private Integer isom49;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameEnglish() {
        return nameEnglish;
    }

    public void setNameEnglish(String nameEnglish) {
        this.nameEnglish = nameEnglish;
    }

    public String getNameLocal() {
        return nameLocal;
    }

    public void setNameLocal(String nameLocal) {
        this.nameLocal = nameLocal;
    }

    public String getIsocode2() {
        return isocode2;
    }

    public void setIsocode2(String isocode2) {
        this.isocode2 = isocode2;
    }

    public String getIsocode3() {
        return isocode3;
    }

    public void setIsocode3(String isocode3) {
        this.isocode3 = isocode3;
    }

    public Integer getIsom49() {
        return isom49;
    }

    public void setIsom49(Integer isom49) {
        this.isom49 = isom49;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Country))
            return false;

        Country country = (Country) o;

        return id != null ? id.equals(country.id) : country.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static final class Builder {
        private Long id;
        private String nameEnglish;
        private String nameLocal;
        private String isocode2;
        private String isocode3;
        private Integer isom49;

        private Builder() {
        }

        public static Builder aCountry() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder nameEnglish(String nameEnglish) {
            this.nameEnglish = nameEnglish;
            return this;
        }

        public Builder nameLocal(String nameLocal) {
            this.nameLocal = nameLocal;
            return this;
        }

        public Builder isocode2(String isocode2) {
            this.isocode2 = isocode2;
            return this;
        }

        public Builder isocode3(String isocode3) {
            this.isocode3 = isocode3;
            return this;
        }

        public Builder isom49(Integer isom49) {
            this.isom49 = isom49;
            return this;
        }

        public Country build() {
            Country country = new Country();
            country.setId(id);
            country.setNameEnglish(nameEnglish);
            country.setNameLocal(nameLocal);
            country.setIsocode2(isocode2);
            country.setIsocode3(isocode3);
            country.setIsom49(isom49);
            return country;
        }
    }

}