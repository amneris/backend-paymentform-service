package com.abaenglish.paymentform.domain;

import com.abaenglish.boot.domain.DomainObject;

import javax.persistence.*;

@Entity
@Table(name = "payment_form")
public class PaymentForm extends DomainObject {
    private static final long serialVersionUID = 4105507090749031618L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "country_iso3", insertable = true, updatable = true, nullable = false, length = 3)
    private String countryIso3;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "method_id", insertable = true, updatable = true, nullable = false)
    private PaymentMethod paymentMethod;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "gateway_id", insertable = true, updatable = true, nullable = false)
    private PaymentGateway paymentGateway;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "provider_id", insertable = true, updatable = true, nullable = false)
    private PaymentProvider paymentProvider;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "hostedpages_id", insertable = true, updatable = true, nullable = false)
    private PaymentHostedPages paymentHostedPages;
    @Column(name = "environmenturl", insertable = true, updatable = true, nullable = false)
    private String environmentUrl;
    @Column(name = "defaultoption", nullable = false)
    private Boolean defaultOption;
    @Column(name = "payment_order", nullable = false)
    private Integer paymentOrder;
    @Column(name = "active", nullable = false)
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryIso3() {
        return countryIso3;
    }

    public void setCountryIso3(String countryIso3) {
        this.countryIso3 = countryIso3;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentGateway getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public PaymentProvider getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public PaymentHostedPages getPaymentHostedPages() {
        return paymentHostedPages;
    }

    public void setPaymentHostedPages(PaymentHostedPages paymentHostedPages) {
        this.paymentHostedPages = paymentHostedPages;
    }

    public String getEnvironmentUrl() {
        return environmentUrl;
    }

    public void setEnvironmentUrl(String environmentUrl) {
        this.environmentUrl = environmentUrl;
    }

    public Boolean getDefaultOption() {
        return defaultOption;
    }

    public void setDefaultOption(Boolean defaultOption) {
        this.defaultOption = defaultOption;
    }

    public Integer getPaymentOrder() {
        return paymentOrder;
    }

    public void setPaymentOrder(Integer paymentOrder) {
        this.paymentOrder = paymentOrder;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PaymentForm))
            return false;

        PaymentForm that = (PaymentForm) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static final class Builder {
        private Long id;
        private String countryIso3;
        private PaymentMethod paymentMethod;
        private PaymentGateway paymentGateway;
        private PaymentProvider paymentProvider;
        private PaymentHostedPages paymentHostedPages;
        private String environmentUrl;
        private Boolean defaultOption;
        private Integer paymentOrder;
        private Boolean active;

        private Builder() {
        }

        public static Builder aPaymentForm() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder countryIso3(String countryIso3) {
            this.countryIso3 = countryIso3;
            return this;
        }

        public Builder paymentMethod(PaymentMethod paymentMethod) {
            this.paymentMethod = paymentMethod;
            return this;
        }

        public Builder paymentGateway(PaymentGateway paymentGateway) {
            this.paymentGateway = paymentGateway;
            return this;
        }

        public Builder paymentProvider(PaymentProvider paymentProvider) {
            this.paymentProvider = paymentProvider;
            return this;
        }

        public Builder paymentHostedPages(PaymentHostedPages paymentHostedPages) {
            this.paymentHostedPages = paymentHostedPages;
            return this;
        }

        public Builder environmentUrl(String environmentUrl) {
            this.environmentUrl = environmentUrl;
            return this;
        }

        public Builder defaultOption(Boolean defaultOption) {
            this.defaultOption = defaultOption;
            return this;
        }

        public Builder paymentOrder(Integer paymentOrder) {
            this.paymentOrder = paymentOrder;
            return this;
        }

        public Builder active(Boolean active) {
            this.active = active;
            return this;
        }

        public PaymentForm build() {
            PaymentForm paymentForm = new PaymentForm();
            paymentForm.setId(id);
            paymentForm.setCountryIso3(countryIso3);
            paymentForm.setPaymentMethod(paymentMethod);
            paymentForm.setPaymentGateway(paymentGateway);
            paymentForm.setPaymentProvider(paymentProvider);
            paymentForm.setPaymentHostedPages(paymentHostedPages);
            paymentForm.setEnvironmentUrl(environmentUrl);
            paymentForm.setDefaultOption(defaultOption);
            paymentForm.setPaymentOrder(paymentOrder);
            paymentForm.setActive(active);
            return paymentForm;
        }
    }
}