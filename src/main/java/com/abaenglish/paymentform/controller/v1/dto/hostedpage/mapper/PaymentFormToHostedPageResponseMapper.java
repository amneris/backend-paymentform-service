package com.abaenglish.paymentform.controller.v1.dto.hostedpage.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.paymentform.controller.v1.dto.hostedpage.HostedPageResponse;
import com.abaenglish.paymentform.domain.PaymentForm;
import org.springframework.stereotype.Component;

@Component
public class PaymentFormToHostedPageResponseMapper extends BaseCustomMapper<PaymentForm, HostedPageResponse> {
    public PaymentFormToHostedPageResponseMapper() {
        super();
        addField("paymentHostedPages.id", "methodId");
        addField("paymentHostedPages.pageId", "pageId");
        addField("paymentMethod.name", "methodName");
        addField("paymentProvider.name", "providerName");
        addField("paymentGateway.name", "gatewayName");
    }
}
