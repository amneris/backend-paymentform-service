package com.abaenglish.paymentform.controller.v1.dto.hostedpage;

public class HostedPageResponse {

    private Integer methodId;
    private String pageId;
    private String methodName;   //Sanitice
    private String providerName;
    private String gatewayName;

    public Integer getMethodId() {
        return methodId;
    }

    public void setMethodId(Integer methodId) {
        this.methodId = methodId;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public static final class Builder {
        private Integer hostedPageId;
        private String pageId;
        private String methodName;   //Sanitice
        private String providerName;
        private String gatewayName;

        private Builder() {
        }

        public static Builder aHostedPagesResponse() {
            return new Builder();
        }

        public Builder hostedPageId(Integer hostedPageId) {
            this.hostedPageId = hostedPageId;
            return this;
        }

        public Builder pageId(String pageId) {
            this.pageId = pageId;
            return this;
        }

        public Builder methodName(String methodName) {
            this.methodName = methodName;
            return this;
        }

        public Builder providerName(String providerName) {
            this.providerName = providerName;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public HostedPageResponse build() {
            HostedPageResponse hostedPagesResponse = new HostedPageResponse();
            hostedPagesResponse.setMethodId(hostedPageId);
            hostedPagesResponse.setPageId(pageId);
            hostedPagesResponse.setMethodName(methodName);
            hostedPagesResponse.setProviderName(providerName);
            hostedPagesResponse.setGatewayName(gatewayName);
            return hostedPagesResponse;
        }
    }
}
