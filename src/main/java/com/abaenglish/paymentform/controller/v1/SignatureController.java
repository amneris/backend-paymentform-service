package com.abaenglish.paymentform.controller.v1;

import com.abaenglish.external.zuora.rest.domain.signature.ResponseSignature;
import com.abaenglish.paymentform.service.ISignatureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "signatures", description = "Operations about signature")
@RestController
@RequestMapping(value = "api/v1/signatures", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SignatureController {

    @Autowired
    private ISignatureService signatureService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Obtain Signature", response = ResponseSignature.class)
    public ResponseSignature obtainSignature(
            @RequestParam("page") String page) {

        return signatureService.getSignature(page);
    }
}