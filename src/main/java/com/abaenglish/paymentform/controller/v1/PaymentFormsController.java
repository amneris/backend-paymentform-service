package com.abaenglish.paymentform.controller.v1;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.paymentform.controller.v1.dto.hostedpage.HostedPageResponse;
import com.abaenglish.paymentform.exception.ErrorMessages;
import com.abaenglish.paymentform.service.IPaymentFormsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Api(value = "Hosted pages", description = "Operations about hosted pages")
@RestController
@RequestMapping(value = "api/v1/paymentforms", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PaymentFormsController {

    @Autowired
    private IPaymentFormsService paymentformsService;

    @Autowired
    private OrikaBeanMapper mapper;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Obtain All hosted Pages", notes = "This can only be done with well-formed JSON")
    public List<HostedPageResponse> getHostedPages(@RequestParam("user") Long userId,
                       @RequestHeader(value="Origin", required=false) String Origin) throws ApiException {

        String url = null;
        try {
            url = new URL(Origin).getHost();
        } catch (MalformedURLException e) {
            throw new BadRequestApiException(ErrorMessages.BAD_ORIGIN.getError());
        }

        return mapper.mapAsList(paymentformsService.getHostedPages(userId, url),
                HostedPageResponse.class);
    }

}
