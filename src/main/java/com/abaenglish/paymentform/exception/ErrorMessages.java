package com.abaenglish.paymentform.exception;

import com.abaenglish.boot.exception.CodeMessage;

public enum ErrorMessages {

    // They stay in Subscription Service:
    // SUB0003, SUB0004, SUB0005, SUB0006, SUB0007, SUB0008, SUB0009, SUB00010, SUB0011, SUB0013, SUB0014

    BAD_ORIGIN(new CodeMessage("SUB0001", "Uknown/bad origin")),
    ENVIRONMENT_NOT_FOUND(new CodeMessage("SUB0002", "Environment not found")),
    COUNTRY_NOT_FOUND(new CodeMessage("SUB0012", "Country not found"));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
