package com.abaenglish.paymentform.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "paymentform.hostedpage")
public class SubscriptionProperties {

    private String defaultcountryiso3;

    public String getDefaultcountryiso3() {
        return defaultcountryiso3;
    }

    public void setDefaultcountryiso3(String defaultcountryiso3) {
        this.defaultcountryiso3 = defaultcountryiso3;
    }
}
