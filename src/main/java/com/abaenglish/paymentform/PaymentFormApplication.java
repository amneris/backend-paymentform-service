package com.abaenglish.paymentform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentFormApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentFormApplication.class, args); //NOSONAR
    }

}
