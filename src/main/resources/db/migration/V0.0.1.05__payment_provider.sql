CREATE TABLE payment_provider (
  id          BIGINT PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(100) NOT NULL,
  active      BOOLEAN
);

insert into payment_provider(id, name, active) values (1, 'Zuora', TRUE);
insert into payment_provider(id, name, active) values (2, 'PayPal', TRUE);
