CREATE TABLE country (
  id          BIGINT PRIMARY KEY AUTO_INCREMENT,
  name_english        VARCHAR(100) NOT NULL,
  name_local          VARCHAR(100) DEFAULT NULL,
  isocode2     VARCHAR(2) NOT NULL,
  isocode3     VARCHAR(3) NOT NULL ,
  isom49      INTEGER NOT NULL
);

ALTER TABLE country ADD CONSTRAINT uq_isocode3 UNIQUE (isocode3);

insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (1, 'Default', 'Default', 'AB', 'ABA', 1);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (2, 'Andorra', 'Andorra', 'AD', 'AND', 20);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (3, 'United Arab Emirates', 'Al Imarat al Arabiyah al Muttahidah', 'AE', 'ARE', 784);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (4, 'Afghanistan', 'Afghanestan', 'AF', 'AFG', 4);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (5, 'Antigua and Barbuda', 'Antigua and Barbuda', 'AG', 'ATG', 28);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (6, 'Anguilla', 'Anguilla', 'AI', 'AIA', 660);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (7, 'Albania', 'Shqiperia', 'AL', 'ALB', 8);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (8, 'Armenia', 'Hayastan', 'AM', 'ARM', 51);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (9, 'Angola', 'Angola', 'AO', 'AGO', 24);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (10, 'Antarctica', 'Antarctica', 'AQ', 'ATA', 10);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (11, 'Argentina', 'Argentina', 'AR', 'ARG', 32);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (12, 'American Samoa', 'American Samoa', 'AS', 'ASM', 16);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (13, 'Austria', 'Österreich', 'AT', 'AUT', 40);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (14, 'Australia', 'Australia', 'AU', 'AUS', 36);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (15, 'Aruba', 'Aruba', 'AW', 'ABW', 533);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (16, 'Aland Islands', 'Landskapet Åland', 'AX', 'ALA', 248);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (17, 'Azerbaijan', 'Azarbaycan', 'AZ', 'AZE', 31);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (18, 'Bosnia and Herzegovina', 'Bosna i Hercegovina', 'BA', 'BIH', 70);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (19, 'Barbados', 'Barbados', 'BB', 'BRB', 52);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (20, 'Bangladesh', 'Bangladesh', 'BD', 'BGD', 50);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (21, 'Belgium', 'Belgique/Belgie', 'BE', 'BEL', 56);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (22, 'Burkina Faso', 'Burkina Faso', 'BF', 'BFA', 854);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (23, 'Bulgaria', 'Bulgaria', 'BG', 'BGR', 100);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (24, 'Bahrain', 'Al Bahrayn', 'BH', 'BHR', 48);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (25, 'Burundi', 'Burundi', 'BI', 'BDI', 108);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (26, 'Benin', 'Benin', 'BJ', 'BEN', 204);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (27, 'Saint Barthelemy', 'Saint-Barthélemy', 'BL', 'BLM', 652);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (28, 'Bermuda', 'Bermuda', 'BM', 'BMU', 60);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (29, 'Brunei', 'Negara Brunei Darussalam', 'BN', 'BRN', 96);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (30, 'Bolivia', 'Bolivia', 'BO', 'BOL', 68);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (31, 'Bonaire, Saint Eustatius and Saba ', 'Bonaire', 'BQ', 'BES', 535);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (32, 'Brazil', 'Brasil', 'BR', 'BRA', 76);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (33, 'Bahamas', 'Bahamas', 'BS', 'BHS', 44);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (34, 'Bhutan', 'Bhutan', 'BT', 'BTN', 64);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (35, 'Bouvet Island', 'Bouvetøya', 'BV', 'BVT', 74);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (36, 'Botswana', 'Botswana', 'BW', 'BWA', 72);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (37, 'Belarus', 'Byelarus', 'BY', 'BLR', 112);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (38, 'Belize', 'Belize', 'BZ', 'BLZ', 84);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (39, 'Canada', 'Canada', 'CA', 'CAN', 124);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (40, 'Cocos Islands', 'Cocos (Keeling) Islands', 'CC', 'CCK', 166);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (41, 'Democratic Republic of the Congo', 'République Démocratique du Congo', 'CD', 'COD', 180);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (42, 'Central African Republic', 'Republique Centrafricaine', 'CF', 'CAF', 140);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (43, 'Republic of the Congo', 'République du Congo', 'CG', 'COG', 178);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (44, 'Switzerland', 'Schweiz (German), Suisse (French), Svizzera (Italian)', 'CH', 'CHE', 756);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (45, 'Ivory Coast', 'Cote d''Ivoire', 'CI', 'CIV', 384);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (46, 'Cook Islands', 'Cook Islands', 'CK', 'COK', 184);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (47, 'Chile', 'Chile', 'CL', 'CHL', 152);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (48, 'Cameroon', 'Cameroon', 'CM', 'CMR', 120);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (49, 'China', 'Zhong Guo', 'CN', 'CHN', 156);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (50, 'Colombia', 'Colombia', 'CO', 'COL', 170);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (51, 'Costa Rica', 'Costa Rica', 'CR', 'CRI', 188);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (52, 'Cuba', 'Cuba', 'CU', 'CUB', 192);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (53, 'Cape Verde', 'Cabo Verde', 'CV', 'CPV', 132);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (54, 'Curacao', 'Curaçao', 'CW', 'CUW', 531);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (55, 'Christmas Island', 'Christmas Island', 'CX', 'CXR', 162);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (56, 'Cyprus', 'Kibris, Kypros', 'CY', 'CYP', 196);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (57, 'Czech Republic', 'Ceska Republika', 'CZ', 'CZE', 203);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (58, 'Germany', 'Deutschland', 'DE', 'DEU', 276);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (59, 'Djibouti', 'Djibouti', 'DJ', 'DJI', 262);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (60, 'Denmark', 'Danmark', 'DK', 'DNK', 208);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (61, 'Dominica', 'Dominica', 'DM', 'DMA', 212);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (62, 'Dominican Republic', 'Dominicana, Republica', 'DO', 'DOM', 214);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (63, 'Algeria', 'Al Jaza''ir', 'DZ', 'DZA', 12);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (64, 'Ecuador', 'Ecuador', 'EC', 'ECU', 218);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (65, 'Estonia', 'Eesti Vabariik', 'EE', 'EST', 233);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (66, 'Egypt', 'Misr', 'EG', 'EGY', 818);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (67, 'Western Sahara', 'Aṣ-Ṣaḥrā’ al-Gharbīyah', 'EH', 'ESH', 732);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (68, 'Eritrea', 'Hagere Ertra', 'ER', 'ERI', 232);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (69, 'Spain', 'España', 'ES', 'ESP', 724);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (70, 'Ethiopia', 'Ityop''iya', 'ET', 'ETH', 231);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (71, 'Finland', 'Suomen Tasavalta', 'FI', 'FIN', 246);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (72, 'Fiji', 'Fiji', 'FJ', 'FJI', 242);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (73, 'Falkland Islands', 'Islas Malvinas', 'FK', 'FLK', 238);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (74, 'Micronesia', 'Micronesia', 'FM', 'FSM', 583);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (75, 'Faroe Islands', 'Foroyar', 'FO', 'FRO', 234);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (76, 'France', 'France', 'FR', 'FRA', 250);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (77, 'Gabon', 'Gabon', 'GA', 'GAB', 266);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (78, 'United Kingdom', 'United Kingdom', 'GB', 'GBR', 826);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (79, 'Grenada', 'Grenada', 'GD', 'GRD', 308);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (80, 'Georgia', 'Sak''art''velo', 'GE', 'GEO', 268);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (81, 'French Guiana', 'Guyane', 'GF', 'GUF', 254);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (82, 'Guernsey', 'Guernsey', 'GG', 'GGY', 831);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (83, 'Ghana', 'Ghana', 'GH', 'GHA', 288);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (84, 'Gibraltar', 'Gibraltar', 'GI', 'GIB', 292);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (85, 'Greenland', 'Kalaallit Nunaat', 'GL', 'GRL', 304);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (86, 'Gambia', 'The Gambia', 'GM', 'GMB', 270);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (87, 'Guinea', 'Guinee', 'GN', 'GIN', 324);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (88, 'Guadeloupe', 'Guadeloupe', 'GP', 'GLP', 312);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (89, 'Equatorial Guinea', 'Guinea Ecuatorial', 'GQ', 'GNQ', 226);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (90, 'Greece', 'Ellas or Ellada', 'GR', 'GRC', 300);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (91, 'South Georgia and the South Sandwich Islands', '', 'GS', 'SGS', 239);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (92, 'Guatemala', 'Guatemala', 'GT', 'GTM', 320);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (93, 'Guam', 'Guam', 'GU', 'GUM', 316);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (94, 'Guinea-Bissau', 'Guine-Bissau', 'GW', 'GNB', 624);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (95, 'Guyana', 'Guyana', 'GY', 'GUY', 328);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (96, 'Hong Kong', 'Xianggang', 'HK', 'HKG', 344);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (97, 'Heard Island and McDonald Islands', '', 'HM', 'HMD', 334);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (98, 'Honduras', 'Honduras', 'HN', 'HND', 340);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (99, 'Croatia', 'Hrvatska', 'HR', 'HRV', 191);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (100, 'Haiti', 'Haiti', 'HT', 'HTI', 332);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (101, 'Hungary', 'Magyarorszag', 'HU', 'HUN', 348);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (102, 'Indonesia', 'Indonesia', 'ID', 'IDN', 360);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (103, 'Ireland', 'Éire', 'IE', 'IRL', 372);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (104, 'Israel', 'Yisra''el', 'IL', 'ISR', 376);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (105, 'Isle of Man', 'Isle of Man', 'IM', 'IMN', 833);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (106, 'India', 'Bharat', 'IN', 'IND', 356);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (107, 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IO', 'IOT', 86);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (108, 'Iraq', 'Iraq', 'IQ', 'IRQ', 368);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (109, 'Iran', 'Iran', 'IR', 'IRN', 364);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (110, 'Iceland', 'Lyoveldio Island', 'IS', 'ISL', 352);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (111, 'Italy', 'Italia', 'IT', 'ITA', 380);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (112, 'Jersey', 'Jersey', 'JE', 'JEY', 832);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (113, 'Jamaica', 'Jamaica', 'JM', 'JAM', 388);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (114, 'Jordan', 'Al Urdun', 'JO', 'JOR', 400);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (115, 'Japan', 'Nippon', 'JP', 'JPN', 392);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (116, 'Kenya', 'Kenya', 'KE', 'KEN', 404);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (117, 'Kyrgyzstan', 'Kyrgyz Respublikasy', 'KG', 'KGZ', 417);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (118, 'Cambodia', 'Kampuchea', 'KH', 'KHM', 116);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (119, 'Kiribati', 'Kiribati, Kiribas', 'KI', 'KIR', 296);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (120, 'Comoros', 'Comores', 'KM', 'COM', 174);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (121, 'Saint Kitts and Nevis', 'Saint Kitts and Nevis', 'KN', 'KNA', 659);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (122, 'North Korea', 'Choson', 'KP', 'PRK', 408);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (123, 'South Korea', 'Han-guk', 'KR', 'KOR', 410);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (124, 'Kosovo', 'Kosova (Albanian), Kosovo (Serbian)', 'XK', 'XKX', 0);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (125, 'Kuwait', 'Al Kuwayt', 'KW', 'KWT', 414);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (126, 'Cayman Islands', 'Cayman Islands', 'KY', 'CYM', 136);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (127, 'Kazakhstan', 'Qazaqstan', 'KZ', 'KAZ', 398);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (128, 'Laos', 'Lao', 'LA', 'LAO', 418);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (129, 'Lebanon', 'Lubnan', 'LB', 'LBN', 422);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (130, 'Saint Lucia', 'Saint Lucia', 'LC', 'LCA', 662);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (131, 'Liechtenstein', 'Liechtenstein', 'LI', 'LIE', 438);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (132, 'Sri Lanka', 'Sri Lanka', 'LK', 'LKA', 144);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (133, 'Liberia', 'Liberia', 'LR', 'LBR', 430);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (134, 'Lesotho', 'Lesotho', 'LS', 'LSO', 426);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (135, 'Lithuania', 'Lietuva', 'LT', 'LTU', 440);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (136, 'Luxembourg', 'Luxembourg, Letzebuerg', 'LU', 'LUX', 442);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (137, 'Latvia', 'Latvija', 'LV', 'LVA', 428);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (138, 'Libya', 'Libiyah', 'LY', 'LBY', 434);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (139, 'Morocco', 'Al Maghrib', 'MA', 'MAR', 504);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (140, 'Monaco', 'Monaco', 'MC', 'MCO', 492);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (141, 'Moldova', 'Moldova', 'MD', 'MDA', 498);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (142, 'Montenegro', 'Crna Gora', 'ME', 'MNE', 499);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (143, 'Saint Martin', 'Saint-Martin', 'MF', 'MAF', 663);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (144, 'Madagascar', 'Madagascar', 'MG', 'MDG', 450);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (145, 'Marshall Islands', 'Marshall Islands', 'MH', 'MHL', 584);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (146, 'Macedonia', 'Makedonija', 'MK', 'MKD', 807);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (147, 'Mali', 'Mali', 'ML', 'MLI', 466);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (148, 'Myanmar', 'Myanma Naingngandaw', 'MM', 'MMR', 104);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (149, 'Mongolia', 'Mongol Uls', 'MN', 'MNG', 496);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (150, 'Macao', 'Aomen', 'MO', 'MAC', 446);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (151, 'Northern Mariana Islands', 'Northern Mariana Islands', 'MP', 'MNP', 580);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (152, 'Martinique', 'Martinique', 'MQ', 'MTQ', 474);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (153, 'Mauritania', 'Muritaniyah', 'MR', 'MRT', 478);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (154, 'Montserrat', 'Montserrat', 'MS', 'MSR', 500);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (155, 'Malta', 'Malta', 'MT', 'MLT', 470);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (156, 'Mauritius', 'Mauritius', 'MU', 'MUS', 480);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (157, 'Maldives', 'Dhivehi Raajje', 'MV', 'MDV', 462);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (158, 'Malawi', 'Malawi', 'MW', 'MWI', 454);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (159, 'Mexico', 'Estados Unidos Mexicanos', 'MX', 'MEX', 484);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (160, 'Malaysia', 'Malaysia', 'MY', 'MYS', 458);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (161, 'Mozambique', 'Mocambique', 'MZ', 'MOZ', 508);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (162, 'Namibia', 'Namibia', 'NA', 'NAM', 516);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (163, 'New Caledonia', 'Nouvelle-Calédonie', 'NC', 'NCL', 540);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (164, 'Niger', 'Niger', 'NE', 'NER', 562);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (165, 'Norfolk Island', 'Norfolk Island', 'NF', 'NFK', 574);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (166, 'Nigeria', 'Nigeria', 'NG', 'NGA', 566);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (167, 'Nicaragua', 'Nicaragua', 'NI', 'NIC', 558);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (168, 'Netherlands', 'Nederland/Holland', 'NL', 'NLD', 528);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (169, 'Norway', 'Norge', 'NO', 'NOR', 578);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (170, 'Nepal', 'Nepal', 'NP', 'NPL', 524);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (171, 'Nauru', 'Nauru', 'NR', 'NRU', 520);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (172, 'Niue', 'Niue', 'NU', 'NIU', 570);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (173, 'New Zealand', 'Aotearoa', 'NZ', 'NZL', 554);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (174, 'Oman', 'Saltanat Uman', 'OM', 'OMN', 512);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (175, 'Panama', 'Panama', 'PA', 'PAN', 591);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (176, 'Peru', 'Peru', 'PE', 'PER', 604);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (177, 'French Polynesia', 'Polynésie Française', 'PF', 'PYF', 258);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (178, 'Papua New Guinea', 'Papua Niu Gini', 'PG', 'PNG', 598);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (179, 'Philippines', 'Pilipinas', 'PH', 'PHL', 608);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (180, 'Pakistan', 'Pakistan', 'PK', 'PAK', 586);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (181, 'Poland', 'Polska', 'PL', 'POL', 616);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (182, 'Saint Pierre and Miquelon', 'Saint-Pierre et Miquelon', 'PM', 'SPM', 666);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (183, 'Pitcairn', 'Pitcairn', 'PN', 'PCN', 612);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (184, 'Puerto Rico', 'Puerto Rico', 'PR', 'PRI', 630);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (185, 'Palestinian Territory', 'Filastin', 'PS', 'PSE', 275);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (186, 'Portugal', 'Portugal', 'PT', 'PRT', 620);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (187, 'Palau', 'Belau', 'PW', 'PLW', 585);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (188, 'Paraguay', 'Paraguay', 'PY', 'PRY', 600);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (189, 'Qatar', 'Dawlat Qatar', 'QA', 'QAT', 634);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (190, 'Reunion', 'Ile de la Réunion', 'RE', 'REU', 638);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (191, 'Romania', 'Romania', 'RO', 'ROU', 642);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (192, 'Serbia', 'Srbija', 'RS', 'SRB', 688);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (193, 'Russia', 'Rossiya', 'RU', 'RUS', 643);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (194, 'Rwanda', 'Rwanda', 'RW', 'RWA', 646);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (195, 'Saudi Arabia', 'Al Arabiyah as Suudiyah', 'SA', 'SAU', 682);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (196, 'Solomon Islands', 'Solomon Islands', 'SB', 'SLB', 90);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (197, 'Seychelles', 'Seychelles', 'SC', 'SYC', 690);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (198, 'Sudan', 'As-Sudan', 'SD', 'SDN', 729);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (199, 'South Sudan', 'South Sudan', 'SS', 'SSD', 728);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (200, 'Sweden', 'Sverige', 'SE', 'SWE', 752);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (201, 'Singapore', 'Singapore', 'SG', 'SGP', 702);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (202, 'Saint Helena', 'Saint Helena', 'SH', 'SHN', 654);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (203, 'Slovenia', 'Slovenija', 'SI', 'SVN', 705);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (204, 'Svalbard and Jan Mayen', 'Svalbard', 'SJ', 'SJM', 744);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (205, 'Slovakia', 'Slovensko', 'SK', 'SVK', 703);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (206, 'Sierra Leone', 'Sierra Leone', 'SL', 'SLE', 694);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (207, 'San Marino', 'San Marino', 'SM', 'SMR', 674);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (208, 'Senegal', 'Senegal', 'SN', 'SEN', 686);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (209, 'Somalia', 'Somalia', 'SO', 'SOM', 706);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (210, 'Suriname', 'Suriname', 'SR', 'SUR', 740);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (211, 'Sao Tome and Principe', 'Sao Tome e Principe', 'ST', 'STP', 678);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (212, 'El Salvador', 'El Salvador', 'SV', 'SLV', 222);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (213, 'Sint Maarten', 'Sint Maarten', 'SX', 'SXM', 534);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (214, 'Syria', 'Suriyah', 'SY', 'SYR', 760);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (215, 'Swaziland', 'Swaziland', 'SZ', 'SWZ', 748);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (216, 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TC', 'TCA', 796);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (217, 'Chad', 'Tchad', 'TD', 'TCD', 148);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (218, 'French Southern Territories', 'Territoire des Terres australes et antarctiques françaises', 'TF', 'ATF', 260);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (219, 'Togo', 'Republique Togolaise', 'TG', 'TGO', 768);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (220, 'Thailand', 'Prathet Thai', 'TH', 'THA', 764);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (221, 'Tajikistan', 'Jumhurii Tojikiston', 'TJ', 'TJK', 762);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (222, 'Tokelau', 'Tokelau', 'TK', 'TKL', 772);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (223, 'East Timor', 'Timor', 'TL', 'TLS', 626);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (224, 'Turkmenistan', 'Turkmenistan', 'TM', 'TKM', 795);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (225, 'Tunisia', 'Tunis', 'TN', 'TUN', 788);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (226, 'Tonga', 'Tonga', 'TO', 'TON', 776);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (227, 'Turkey', 'Turkiye', 'TR', 'TUR', 792);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (228, 'Trinidad and Tobago', 'Trinidad, Tobago', 'TT', 'TTO', 780);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (229, 'Tuvalu', 'Tuvalu', 'TV', 'TUV', 798);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (230, 'Taiwan', 'T''ai-wan', 'TW', 'TWN', 158);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (231, 'Tanzania', 'Jamhuri ya Muungano wa Tanzania', 'TZ', 'TZA', 834);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (232, 'Ukraine', 'Ukrayina', 'UA', 'UKR', 804);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (233, 'Uganda', 'Uganda', 'UG', 'UGA', 800);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (234, 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UM', 'UMI', 581);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (235, 'United States', 'United States', 'US', 'USA', 840);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (236, 'Uruguay', 'Republica Oriental del Uruguay', 'UY', 'URY', 858);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (237, 'Uzbekistan', 'Uzbekiston Respublikasi', 'UZ', 'UZB', 860);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (238, 'Vatican', 'Status Civitatis Vaticanæ', 'VA', 'VAT', 336);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (239, 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VC', 'VCT', 670);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (240, 'Venezuela', 'Venezuela', 'VE', 'VEN', 862);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (241, 'British Virgin Islands', 'Virgin Islands', 'VG', 'VGB', 92);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (242, 'U.S. Virgin Islands', 'Virgin Islands', 'VI', 'VIR', 850);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (243, 'Vietnam', 'Viet Nam', 'VN', 'VNM', 704);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (244, 'Vanuatu', 'Vanuatu', 'VU', 'VUT', 548);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (245, 'Wallis and Futuna', 'Wallis et Futuna', 'WF', 'WLF', 876);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (246, 'Samoa', 'Samoa', 'WS', 'WSM', 882);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (247, 'Yemen', 'Al Yaman', 'YE', 'YEM', 887);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (248, 'Mayotte', 'Mayotte', 'YT', 'MYT', 175);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (249, 'South Africa', 'South Africa', 'ZA', 'ZAF', 710);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (250, 'Zambia', 'Zambia', 'ZM', 'ZMB', 894);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (251, 'Zimbabwe', 'Zimbabwe', 'ZW', 'ZWE', 716);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (252, 'Serbia and Montenegro', 'Srbija', 'CS', 'SCG', 891);
insert into country(id, name_english, name_local, isocode2, isocode3, isom49) values (253, 'Netherlands Antilles', 'Nederlandse Antillen', 'AN', 'ANT', 530);
