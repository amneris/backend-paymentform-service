CREATE TABLE payment_gateway (
  id          BIGINT PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(100) NOT NULL,
  gateway     VARCHAR(100) NOT NULL,
  active      BOOLEAN
);

insert into payment_gateway(id, name, gateway, active) values (1, 'TestGateway','Test Gateway', TRUE);
insert into payment_gateway(id, name, gateway, active) values (2, 'Stripe Gateway','Stripe v1', TRUE);
insert into payment_gateway(id, name, gateway, active) values (3, 'PayPal Payflow Pro','PayPal Payflow Pro', TRUE);
insert into payment_gateway(id, name, gateway, active) values (4, 'PayPal Adaptive Payments','PayPal(Adaptive Payments)', TRUE);
insert into payment_gateway(id, name, gateway, active) values (5, 'AdyenSoloDISCOVER','Adyen', TRUE);
insert into payment_gateway(id, name, gateway, active) values (6, 'Adyen Test Gateway','Adyen', TRUE);
insert into payment_gateway(id, name, gateway, active) values (7, 'PayPal Express Checkout','PayPal Express Checkout', TRUE);
