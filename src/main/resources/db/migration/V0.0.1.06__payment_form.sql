
CREATE TABLE payment_form (
  id            BIGINT PRIMARY KEY AUTO_INCREMENT,
  country_iso3   VARCHAR(3)   NOT NULL,
  method_id      BIGINT NOT NULL,
  gateway_id     BIGINT NOT NULL,
  provider_id     BIGINT NOT NULL,
  hostedpages_id BIGINT NOT NULL,
  environmenturl VARCHAR(100) NOT NULL,
  defaultoption BOOLEAN DEFAULT FALSE,
  payment_order INTEGER DEFAULT 0,
  active        BOOLEAN DEFAULT TRUE
);

ALTER TABLE payment_form ADD CONSTRAINT fk_country_iso3 FOREIGN KEY (country_iso3) REFERENCES country (isocode3);
ALTER TABLE payment_form ADD CONSTRAINT fk_method_id FOREIGN KEY (method_id) REFERENCES payment_method (id);
ALTER TABLE payment_form ADD CONSTRAINT fk_gateway_id FOREIGN KEY (gateway_id) REFERENCES payment_gateway (id);
ALTER TABLE payment_form ADD CONSTRAINT fk_provider_id FOREIGN KEY (provider_id) REFERENCES payment_provider (id);
ALTER TABLE payment_form ADD CONSTRAINT fk_hostedpages_id FOREIGN KEY (hostedpages_id) REFERENCES payment_hostedpages (id);

-- DEFAULT
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1, 'ABA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2, 'ABA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3, 'ABA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4, 'ABA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);

INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5, 'ABA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6, 'ABA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (7, 'ABA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (8, 'ABA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

-- WORLD

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (9, 'AFG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (10, 'AFG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (11, 'AFG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (12, 'AFG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (13, 'AFG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (14, 'AFG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (15, 'AFG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (16, 'AFG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (17, 'AFG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (18, 'AFG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (19, 'AFG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (20, 'AFG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (33, 'ALB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (34, 'ALB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (35, 'ALB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (36, 'ALB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (37, 'ALB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (38, 'ALB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (39, 'ALB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (40, 'ALB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (41, 'ALB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (42, 'ALB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (43, 'ALB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (44, 'ALB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (57, 'DZA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (58, 'DZA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (59, 'DZA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (60, 'DZA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (61, 'DZA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (62, 'DZA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (63, 'DZA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (64, 'DZA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (65, 'DZA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (66, 'DZA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (67, 'DZA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (68, 'DZA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (81, 'ASM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (82, 'ASM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (83, 'ASM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (84, 'ASM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (85, 'ASM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (86, 'ASM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (87, 'ASM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (88, 'ASM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (89, 'ASM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (90, 'ASM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (91, 'ASM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (92, 'ASM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (105, 'AND', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (106, 'AND', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (107, 'AND', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (108, 'AND', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (109, 'AND', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (110, 'AND', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (111, 'AND', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (112, 'AND', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (113, 'AND', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (114, 'AND', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (115, 'AND', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (116, 'AND', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (129, 'AGO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (130, 'AGO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (131, 'AGO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (132, 'AGO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (133, 'AGO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (134, 'AGO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (135, 'AGO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (136, 'AGO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (137, 'AGO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (138, 'AGO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (139, 'AGO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (140, 'AGO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (153, 'AIA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (154, 'AIA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (155, 'AIA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (156, 'AIA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (157, 'AIA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (158, 'AIA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (159, 'AIA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (160, 'AIA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (161, 'AIA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (162, 'AIA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (163, 'AIA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (164, 'AIA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (177, 'ATA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (178, 'ATA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (179, 'ATA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (180, 'ATA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (181, 'ATA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (182, 'ATA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (183, 'ATA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (184, 'ATA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (185, 'ATA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (186, 'ATA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (187, 'ATA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (188, 'ATA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (201, 'ATG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (202, 'ATG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (203, 'ATG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (204, 'ATG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (205, 'ATG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (206, 'ATG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (207, 'ATG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (208, 'ATG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (209, 'ATG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (210, 'ATG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (211, 'ATG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (212, 'ATG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (225, 'ARG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (226, 'ARG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (227, 'ARG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (228, 'ARG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (229, 'ARG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (230, 'ARG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (231, 'ARG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (232, 'ARG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (233, 'ARG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (234, 'ARG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (235, 'ARG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (236, 'ARG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (257, 'ARM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (258, 'ARM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (259, 'ARM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (260, 'ARM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (261, 'ARM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (262, 'ARM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (263, 'ARM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (264, 'ARM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (265, 'ARM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (266, 'ARM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (267, 'ARM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (268, 'ARM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (281, 'ABW', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (282, 'ABW', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (283, 'ABW', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (284, 'ABW', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (285, 'ABW', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (286, 'ABW', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (287, 'ABW', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (288, 'ABW', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (289, 'ABW', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (290, 'ABW', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (291, 'ABW', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (292, 'ABW', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (305, 'AUS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (306, 'AUS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (307, 'AUS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (308, 'AUS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (309, 'AUS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (310, 'AUS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (311, 'AUS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (312, 'AUS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (313, 'AUS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (314, 'AUS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (315, 'AUS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (316, 'AUS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (329, 'AUT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (330, 'AUT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (331, 'AUT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (332, 'AUT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (333, 'AUT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (334, 'AUT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (335, 'AUT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (336, 'AUT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (337, 'AUT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (338, 'AUT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (339, 'AUT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (340, 'AUT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (345, 'AUT', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (346, 'AUT', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (347, 'AUT', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (348, 'AUT', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (361, 'AZE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (362, 'AZE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (363, 'AZE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (364, 'AZE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (365, 'AZE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (366, 'AZE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (367, 'AZE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (368, 'AZE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (369, 'AZE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (370, 'AZE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (371, 'AZE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (372, 'AZE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (385, 'BHS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (386, 'BHS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (387, 'BHS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (388, 'BHS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (389, 'BHS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (390, 'BHS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (391, 'BHS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (392, 'BHS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (393, 'BHS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (394, 'BHS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (395, 'BHS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (396, 'BHS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (409, 'BHR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (410, 'BHR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (411, 'BHR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (412, 'BHR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (413, 'BHR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (414, 'BHR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (415, 'BHR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (416, 'BHR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (417, 'BHR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (418, 'BHR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (419, 'BHR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (420, 'BHR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (433, 'BGD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (434, 'BGD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (435, 'BGD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (436, 'BGD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (437, 'BGD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (438, 'BGD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (439, 'BGD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (440, 'BGD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (441, 'BGD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (442, 'BGD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (443, 'BGD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (444, 'BGD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (457, 'BRB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (458, 'BRB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (459, 'BRB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (460, 'BRB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (461, 'BRB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (462, 'BRB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (463, 'BRB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (464, 'BRB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (465, 'BRB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (466, 'BRB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (467, 'BRB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (468, 'BRB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (481, 'BLR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (482, 'BLR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (483, 'BLR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (484, 'BLR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (485, 'BLR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (486, 'BLR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (487, 'BLR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (488, 'BLR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (489, 'BLR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (490, 'BLR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (491, 'BLR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (492, 'BLR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (505, 'BEL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (506, 'BEL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (507, 'BEL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (508, 'BEL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (509, 'BEL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (510, 'BEL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (511, 'BEL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (512, 'BEL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (513, 'BEL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (514, 'BEL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (515, 'BEL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (516, 'BEL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (521, 'BEL', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (522, 'BEL', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (523, 'BEL', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (524, 'BEL', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (537, 'BLZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (538, 'BLZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (539, 'BLZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (540, 'BLZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (541, 'BLZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (542, 'BLZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (543, 'BLZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (544, 'BLZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (545, 'BLZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (546, 'BLZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (547, 'BLZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (548, 'BLZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (549, 'BLZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (550, 'BLZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (551, 'BLZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (552, 'BLZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (553, 'BLZ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (554, 'BLZ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (555, 'BLZ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (556, 'BLZ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (577, 'BEN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (578, 'BEN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (579, 'BEN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (580, 'BEN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (581, 'BEN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (582, 'BEN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (583, 'BEN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (584, 'BEN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (585, 'BEN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (586, 'BEN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (587, 'BEN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (588, 'BEN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (601, 'BMU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (602, 'BMU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (603, 'BMU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (604, 'BMU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (605, 'BMU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (606, 'BMU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (607, 'BMU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (608, 'BMU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (609, 'BMU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (610, 'BMU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (611, 'BMU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (612, 'BMU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (625, 'BTN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (626, 'BTN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (627, 'BTN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (628, 'BTN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (629, 'BTN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (630, 'BTN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (631, 'BTN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (632, 'BTN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (633, 'BTN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (634, 'BTN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (635, 'BTN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (636, 'BTN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (649, 'BOL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (650, 'BOL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (651, 'BOL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (652, 'BOL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (653, 'BOL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (654, 'BOL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (655, 'BOL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (656, 'BOL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (657, 'BOL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (658, 'BOL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (659, 'BOL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (660, 'BOL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (673, 'BIH', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (674, 'BIH', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (675, 'BIH', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (676, 'BIH', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (677, 'BIH', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (678, 'BIH', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (679, 'BIH', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (680, 'BIH', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (681, 'BIH', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (682, 'BIH', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (683, 'BIH', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (684, 'BIH', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (697, 'BWA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (698, 'BWA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (699, 'BWA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (700, 'BWA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (701, 'BWA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (702, 'BWA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (703, 'BWA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (704, 'BWA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (705, 'BWA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (706, 'BWA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (707, 'BWA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (708, 'BWA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (721, 'BVT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (722, 'BVT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (723, 'BVT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (724, 'BVT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (725, 'BVT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (726, 'BVT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (727, 'BVT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (728, 'BVT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (729, 'BVT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (730, 'BVT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (731, 'BVT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (732, 'BVT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (753, 'BRA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (754, 'BRA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (755, 'BRA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (756, 'BRA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (777, 'IOT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (778, 'IOT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (779, 'IOT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (780, 'IOT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (781, 'IOT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (782, 'IOT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (783, 'IOT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (784, 'IOT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (785, 'IOT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (786, 'IOT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (787, 'IOT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (788, 'IOT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (801, 'BRN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (802, 'BRN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (803, 'BRN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (804, 'BRN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (805, 'BRN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (806, 'BRN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (807, 'BRN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (808, 'BRN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (809, 'BRN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (810, 'BRN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (811, 'BRN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (812, 'BRN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (825, 'BGR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (826, 'BGR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (827, 'BGR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (828, 'BGR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (829, 'BGR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (830, 'BGR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (831, 'BGR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (832, 'BGR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (833, 'BGR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (834, 'BGR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (835, 'BGR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (836, 'BGR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (841, 'BGR', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (842, 'BGR', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (843, 'BGR', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (844, 'BGR', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (857, 'BFA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (858, 'BFA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (859, 'BFA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (860, 'BFA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (861, 'BFA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (862, 'BFA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (863, 'BFA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (864, 'BFA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (865, 'BFA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (866, 'BFA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (867, 'BFA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (868, 'BFA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (881, 'BDI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (882, 'BDI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (883, 'BDI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (884, 'BDI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (885, 'BDI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (886, 'BDI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (887, 'BDI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (888, 'BDI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (889, 'BDI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (890, 'BDI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (891, 'BDI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (892, 'BDI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (905, 'KHM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (906, 'KHM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (907, 'KHM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (908, 'KHM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (909, 'KHM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (910, 'KHM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (911, 'KHM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (912, 'KHM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (913, 'KHM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (914, 'KHM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (915, 'KHM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (916, 'KHM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (929, 'CMR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (930, 'CMR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (931, 'CMR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (932, 'CMR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (933, 'CMR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (934, 'CMR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (935, 'CMR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (936, 'CMR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (937, 'CMR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (938, 'CMR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (939, 'CMR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (940, 'CMR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (953, 'CAN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (954, 'CAN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (955, 'CAN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (956, 'CAN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (957, 'CAN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (958, 'CAN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (959, 'CAN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (960, 'CAN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (961, 'CAN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (962, 'CAN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (963, 'CAN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (964, 'CAN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (985, 'CPV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (986, 'CPV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (987, 'CPV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (988, 'CPV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (989, 'CPV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (990, 'CPV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (991, 'CPV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (992, 'CPV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (993, 'CPV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (994, 'CPV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (995, 'CPV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (996, 'CPV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1009, 'CYM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1010, 'CYM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1011, 'CYM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1012, 'CYM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1013, 'CYM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1014, 'CYM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1015, 'CYM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1016, 'CYM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1017, 'CYM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1018, 'CYM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1019, 'CYM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1020, 'CYM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1033, 'CAF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1034, 'CAF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1035, 'CAF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1036, 'CAF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1037, 'CAF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1038, 'CAF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1039, 'CAF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1040, 'CAF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1041, 'CAF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1042, 'CAF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1043, 'CAF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1044, 'CAF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1057, 'TCD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1058, 'TCD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1059, 'TCD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1060, 'TCD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1061, 'TCD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1062, 'TCD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1063, 'TCD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1064, 'TCD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1065, 'TCD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1066, 'TCD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1067, 'TCD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1068, 'TCD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1081, 'CHL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1082, 'CHL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1083, 'CHL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1084, 'CHL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1085, 'CHL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1086, 'CHL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1087, 'CHL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1088, 'CHL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1089, 'CHL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1090, 'CHL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1091, 'CHL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1092, 'CHL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1105, 'CXR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1106, 'CXR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1107, 'CXR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1108, 'CXR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1109, 'CXR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1110, 'CXR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1111, 'CXR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1112, 'CXR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1113, 'CXR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1114, 'CXR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1115, 'CXR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1116, 'CXR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1129, 'CCK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1130, 'CCK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1131, 'CCK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1132, 'CCK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1133, 'CCK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1134, 'CCK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1135, 'CCK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1136, 'CCK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1137, 'CCK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1138, 'CCK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1139, 'CCK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1140, 'CCK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1153, 'COL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1154, 'COL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1155, 'COL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1156, 'COL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1157, 'COL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1158, 'COL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1159, 'COL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1160, 'COL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1161, 'COL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1162, 'COL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1163, 'COL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1164, 'COL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1177, 'COM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1178, 'COM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1179, 'COM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1180, 'COM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1181, 'COM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1182, 'COM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1183, 'COM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1184, 'COM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1185, 'COM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1186, 'COM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1187, 'COM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1188, 'COM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1201, 'COG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1202, 'COG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1203, 'COG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1204, 'COG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1205, 'COG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1206, 'COG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1207, 'COG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1208, 'COG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1209, 'COG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1210, 'COG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1211, 'COG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1212, 'COG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1225, 'COD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1226, 'COD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1227, 'COD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1228, 'COD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1229, 'COD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1230, 'COD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1231, 'COD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1232, 'COD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1233, 'COD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1234, 'COD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1235, 'COD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1236, 'COD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1249, 'COK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1250, 'COK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1251, 'COK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1252, 'COK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1253, 'COK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1254, 'COK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1255, 'COK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1256, 'COK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1257, 'COK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1258, 'COK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1259, 'COK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1260, 'COK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1273, 'CRI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1274, 'CRI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1275, 'CRI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1276, 'CRI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1277, 'CRI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1278, 'CRI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1279, 'CRI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1280, 'CRI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1281, 'CRI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1282, 'CRI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1283, 'CRI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1284, 'CRI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1305, 'CIV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1306, 'CIV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1307, 'CIV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1308, 'CIV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1309, 'CIV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1310, 'CIV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1311, 'CIV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1312, 'CIV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1313, 'CIV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1314, 'CIV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1315, 'CIV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1316, 'CIV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1329, 'HRV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1330, 'HRV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1331, 'HRV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1332, 'HRV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1333, 'HRV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1334, 'HRV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1335, 'HRV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1336, 'HRV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1337, 'HRV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1338, 'HRV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1339, 'HRV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1340, 'HRV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1345, 'HRV', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1346, 'HRV', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1347, 'HRV', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1348, 'HRV', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1361, 'CUB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1362, 'CUB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1363, 'CUB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1364, 'CUB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1365, 'CUB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1366, 'CUB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1367, 'CUB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1368, 'CUB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1369, 'CUB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1370, 'CUB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1371, 'CUB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1372, 'CUB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1385, 'CYP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1386, 'CYP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1387, 'CYP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1388, 'CYP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1389, 'CYP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1390, 'CYP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1391, 'CYP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1392, 'CYP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1393, 'CYP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1394, 'CYP', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1395, 'CYP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1396, 'CYP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1401, 'CYP', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1402, 'CYP', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1403, 'CYP', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1404, 'CYP', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1417, 'CZE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1418, 'CZE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1419, 'CZE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1420, 'CZE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1421, 'CZE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1422, 'CZE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1423, 'CZE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1424, 'CZE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1425, 'CZE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1426, 'CZE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1427, 'CZE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1428, 'CZE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1433, 'CZE', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1434, 'CZE', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1435, 'CZE', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1436, 'CZE', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1449, 'DNK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1450, 'DNK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1451, 'DNK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1452, 'DNK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1453, 'DNK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1454, 'DNK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1455, 'DNK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1456, 'DNK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1457, 'DNK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1458, 'DNK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1459, 'DNK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1460, 'DNK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1465, 'DNK', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1466, 'DNK', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1467, 'DNK', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1468, 'DNK', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1481, 'DJI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1482, 'DJI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1483, 'DJI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1484, 'DJI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1485, 'DJI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1486, 'DJI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1487, 'DJI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1488, 'DJI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1489, 'DJI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1490, 'DJI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1491, 'DJI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1492, 'DJI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1505, 'DMA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1506, 'DMA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1507, 'DMA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1508, 'DMA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1509, 'DMA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1510, 'DMA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1511, 'DMA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1512, 'DMA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1513, 'DMA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1514, 'DMA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1515, 'DMA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1516, 'DMA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1529, 'DOM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1530, 'DOM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1531, 'DOM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1532, 'DOM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1533, 'DOM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1534, 'DOM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1535, 'DOM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1536, 'DOM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1537, 'DOM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1538, 'DOM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1539, 'DOM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1540, 'DOM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1553, 'ECU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1554, 'ECU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1555, 'ECU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1556, 'ECU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1557, 'ECU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1558, 'ECU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1559, 'ECU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1560, 'ECU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1561, 'ECU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1562, 'ECU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1563, 'ECU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1564, 'ECU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1577, 'EGY', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1578, 'EGY', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1579, 'EGY', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1580, 'EGY', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1581, 'EGY', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1582, 'EGY', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1583, 'EGY', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1584, 'EGY', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1585, 'EGY', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1586, 'EGY', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1587, 'EGY', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1588, 'EGY', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1601, 'SLV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1602, 'SLV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1603, 'SLV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1604, 'SLV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1605, 'SLV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1606, 'SLV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1607, 'SLV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1608, 'SLV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1609, 'SLV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1610, 'SLV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1611, 'SLV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1612, 'SLV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1625, 'GNQ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1626, 'GNQ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1627, 'GNQ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1628, 'GNQ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1629, 'GNQ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1630, 'GNQ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1631, 'GNQ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1632, 'GNQ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1633, 'GNQ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1634, 'GNQ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1635, 'GNQ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1636, 'GNQ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1649, 'ERI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1650, 'ERI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1651, 'ERI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1652, 'ERI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1653, 'ERI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1654, 'ERI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1655, 'ERI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1656, 'ERI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1657, 'ERI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1658, 'ERI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1659, 'ERI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1660, 'ERI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1673, 'EST', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1674, 'EST', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1675, 'EST', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1676, 'EST', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1677, 'EST', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1678, 'EST', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1679, 'EST', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1680, 'EST', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1681, 'EST', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1682, 'EST', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1683, 'EST', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1684, 'EST', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1689, 'EST', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1690, 'EST', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1691, 'EST', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1692, 'EST', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1705, 'ETH', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1706, 'ETH', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1707, 'ETH', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1708, 'ETH', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1709, 'ETH', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1710, 'ETH', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1711, 'ETH', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1712, 'ETH', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1713, 'ETH', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1714, 'ETH', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1715, 'ETH', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1716, 'ETH', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1729, 'FLK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1730, 'FLK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1731, 'FLK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1732, 'FLK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1733, 'FLK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1734, 'FLK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1735, 'FLK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1736, 'FLK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1737, 'FLK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1738, 'FLK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1739, 'FLK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1740, 'FLK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1753, 'FRO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1754, 'FRO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1755, 'FRO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1756, 'FRO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1757, 'FRO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1758, 'FRO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1759, 'FRO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1760, 'FRO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1761, 'FRO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1762, 'FRO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1763, 'FRO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1764, 'FRO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1777, 'FJI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1778, 'FJI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1779, 'FJI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1780, 'FJI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1781, 'FJI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1782, 'FJI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1783, 'FJI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1784, 'FJI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1785, 'FJI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1786, 'FJI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1787, 'FJI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1788, 'FJI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1801, 'FIN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1802, 'FIN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1803, 'FIN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1804, 'FIN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1805, 'FIN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1806, 'FIN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1807, 'FIN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1808, 'FIN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1809, 'FIN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1810, 'FIN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1811, 'FIN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1812, 'FIN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1817, 'FIN', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1818, 'FIN', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1819, 'FIN', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1820, 'FIN', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1833, 'FRA', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1834, 'FRA', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1835, 'FRA', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1836, 'FRA', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1849, 'FRA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1850, 'FRA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1851, 'FRA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1852, 'FRA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1853, 'FRA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1854, 'FRA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1855, 'FRA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1856, 'FRA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1857, 'FRA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1858, 'FRA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1859, 'FRA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1860, 'FRA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1873, 'GUF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1874, 'GUF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1875, 'GUF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1876, 'GUF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1877, 'GUF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1878, 'GUF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1879, 'GUF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1880, 'GUF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1881, 'GUF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1882, 'GUF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1883, 'GUF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1884, 'GUF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1897, 'PYF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1898, 'PYF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1899, 'PYF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1900, 'PYF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1901, 'PYF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1902, 'PYF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1903, 'PYF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1904, 'PYF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1905, 'PYF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1906, 'PYF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1907, 'PYF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1908, 'PYF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1921, 'ATF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1922, 'ATF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1923, 'ATF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1924, 'ATF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1925, 'ATF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1926, 'ATF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1927, 'ATF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1928, 'ATF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1929, 'ATF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1930, 'ATF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1931, 'ATF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1932, 'ATF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1945, 'GAB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1946, 'GAB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1947, 'GAB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1948, 'GAB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1949, 'GAB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1950, 'GAB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1951, 'GAB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1952, 'GAB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1953, 'GAB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1954, 'GAB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1955, 'GAB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1956, 'GAB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1969, 'GMB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1970, 'GMB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1971, 'GMB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1972, 'GMB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1973, 'GMB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1974, 'GMB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1975, 'GMB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1976, 'GMB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1977, 'GMB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1978, 'GMB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1979, 'GMB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1980, 'GMB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1993, 'GEO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1994, 'GEO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1995, 'GEO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1996, 'GEO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1997, 'GEO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1998, 'GEO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (1999, 'GEO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2000, 'GEO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2001, 'GEO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2002, 'GEO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2003, 'GEO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2004, 'GEO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2017, 'DEU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2018, 'DEU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2019, 'DEU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2020, 'DEU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2021, 'DEU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2022, 'DEU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2023, 'DEU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2024, 'DEU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2025, 'DEU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2026, 'DEU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2027, 'DEU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2028, 'DEU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);





  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2065, 'DEU', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2066, 'DEU', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2067, 'DEU', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2068, 'DEU', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2081, 'GHA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2082, 'GHA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2083, 'GHA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2084, 'GHA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2085, 'GHA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2086, 'GHA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2087, 'GHA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2088, 'GHA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2089, 'GHA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2090, 'GHA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2091, 'GHA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2092, 'GHA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2105, 'GIB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2106, 'GIB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2107, 'GIB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2108, 'GIB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2109, 'GIB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2110, 'GIB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2111, 'GIB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2112, 'GIB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2113, 'GIB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2114, 'GIB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2115, 'GIB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2116, 'GIB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2129, 'GRC', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2130, 'GRC', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2131, 'GRC', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2132, 'GRC', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2133, 'GRC', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2134, 'GRC', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2135, 'GRC', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2136, 'GRC', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2137, 'GRC', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2138, 'GRC', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2139, 'GRC', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2140, 'GRC', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2145, 'GRC', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2146, 'GRC', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2147, 'GRC', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2148, 'GRC', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2161, 'GRL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2162, 'GRL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2163, 'GRL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2164, 'GRL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2165, 'GRL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2166, 'GRL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2167, 'GRL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2168, 'GRL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2169, 'GRL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2170, 'GRL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2171, 'GRL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2172, 'GRL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2185, 'GRD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2186, 'GRD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2187, 'GRD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2188, 'GRD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2189, 'GRD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2190, 'GRD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2191, 'GRD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2192, 'GRD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2193, 'GRD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2194, 'GRD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2195, 'GRD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2196, 'GRD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2209, 'GLP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2210, 'GLP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2211, 'GLP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2212, 'GLP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2213, 'GLP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2214, 'GLP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2215, 'GLP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2216, 'GLP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2217, 'GLP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2218, 'GLP', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2219, 'GLP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2220, 'GLP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2233, 'GUM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2234, 'GUM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2235, 'GUM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2236, 'GUM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2237, 'GUM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2238, 'GUM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2239, 'GUM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2240, 'GUM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2241, 'GUM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2242, 'GUM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2243, 'GUM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2244, 'GUM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2257, 'GTM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2258, 'GTM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2259, 'GTM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2260, 'GTM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2261, 'GTM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2262, 'GTM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2263, 'GTM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2264, 'GTM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2265, 'GTM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2266, 'GTM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2267, 'GTM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2268, 'GTM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2281, 'GIN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2282, 'GIN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2283, 'GIN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2284, 'GIN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2285, 'GIN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2286, 'GIN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2287, 'GIN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2288, 'GIN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2289, 'GIN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2290, 'GIN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2291, 'GIN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2292, 'GIN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2305, 'GNB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2306, 'GNB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2307, 'GNB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2308, 'GNB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2309, 'GNB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2310, 'GNB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2311, 'GNB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2312, 'GNB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2313, 'GNB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2314, 'GNB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2315, 'GNB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2316, 'GNB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2329, 'GUY', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2330, 'GUY', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2331, 'GUY', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2332, 'GUY', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2333, 'GUY', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2334, 'GUY', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2335, 'GUY', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2336, 'GUY', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2337, 'GUY', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2338, 'GUY', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2339, 'GUY', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2340, 'GUY', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2353, 'HTI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2354, 'HTI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2355, 'HTI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2356, 'HTI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2357, 'HTI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2358, 'HTI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2359, 'HTI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2360, 'HTI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2361, 'HTI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2362, 'HTI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2363, 'HTI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2364, 'HTI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2377, 'HMD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2378, 'HMD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2379, 'HMD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2380, 'HMD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2381, 'HMD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2382, 'HMD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2383, 'HMD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2384, 'HMD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2385, 'HMD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2386, 'HMD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2387, 'HMD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2388, 'HMD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2401, 'VAT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2402, 'VAT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2403, 'VAT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2404, 'VAT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2405, 'VAT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2406, 'VAT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2407, 'VAT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2408, 'VAT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2409, 'VAT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2410, 'VAT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2411, 'VAT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2412, 'VAT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2425, 'HND', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2426, 'HND', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2427, 'HND', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2428, 'HND', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2429, 'HND', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2430, 'HND', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2431, 'HND', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2432, 'HND', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2433, 'HND', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2434, 'HND', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2435, 'HND', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2436, 'HND', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2449, 'HKG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2450, 'HKG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2451, 'HKG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2452, 'HKG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2453, 'HKG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2454, 'HKG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2455, 'HKG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2456, 'HKG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2457, 'HKG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2458, 'HKG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2459, 'HKG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2460, 'HKG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2473, 'HUN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2474, 'HUN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2475, 'HUN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2476, 'HUN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2477, 'HUN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2478, 'HUN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2479, 'HUN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2480, 'HUN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2481, 'HUN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2482, 'HUN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2483, 'HUN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2484, 'HUN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2489, 'HUN', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2490, 'HUN', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2491, 'HUN', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2492, 'HUN', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2505, 'ISL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2506, 'ISL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2507, 'ISL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2508, 'ISL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2509, 'ISL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2510, 'ISL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2511, 'ISL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2512, 'ISL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2513, 'ISL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2514, 'ISL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2515, 'ISL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2516, 'ISL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2529, 'IND', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2530, 'IND', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2531, 'IND', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2532, 'IND', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2533, 'IND', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2534, 'IND', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2535, 'IND', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2536, 'IND', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2537, 'IND', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2538, 'IND', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2539, 'IND', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2540, 'IND', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2553, 'IRN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2554, 'IRN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2555, 'IRN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2556, 'IRN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2557, 'IRN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2558, 'IRN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2559, 'IRN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2560, 'IRN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2561, 'IRN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2562, 'IRN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2563, 'IRN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2564, 'IRN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2577, 'IRQ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2578, 'IRQ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2579, 'IRQ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2580, 'IRQ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2581, 'IRQ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2582, 'IRQ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2583, 'IRQ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2584, 'IRQ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2585, 'IRQ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2586, 'IRQ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2587, 'IRQ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2588, 'IRQ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2601, 'IRL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2602, 'IRL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2603, 'IRL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2604, 'IRL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2605, 'IRL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2606, 'IRL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2607, 'IRL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2608, 'IRL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2609, 'IRL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2610, 'IRL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2611, 'IRL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2612, 'IRL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2617, 'IRL', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2618, 'IRL', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2619, 'IRL', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2620, 'IRL', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2633, 'ISR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2634, 'ISR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2635, 'ISR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2636, 'ISR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2637, 'ISR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2638, 'ISR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2639, 'ISR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2640, 'ISR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2641, 'ISR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2642, 'ISR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2643, 'ISR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2644, 'ISR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2657, 'ITA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2658, 'ITA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2659, 'ITA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2660, 'ITA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2661, 'ITA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2662, 'ITA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2663, 'ITA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2664, 'ITA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2665, 'ITA', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2666, 'ITA', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2667, 'ITA', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2668, 'ITA', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2673, 'ITA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2674, 'ITA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2675, 'ITA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2676, 'ITA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2689, 'JAM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2690, 'JAM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2691, 'JAM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2692, 'JAM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2693, 'JAM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2694, 'JAM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2695, 'JAM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2696, 'JAM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2697, 'JAM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2698, 'JAM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2699, 'JAM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2700, 'JAM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2713, 'JPN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2714, 'JPN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2715, 'JPN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2716, 'JPN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2717, 'JPN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2718, 'JPN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2719, 'JPN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2720, 'JPN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2721, 'JPN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2722, 'JPN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2723, 'JPN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2724, 'JPN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2737, 'JOR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2738, 'JOR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2739, 'JOR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2740, 'JOR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2741, 'JOR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2742, 'JOR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2743, 'JOR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2744, 'JOR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2745, 'JOR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2746, 'JOR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2747, 'JOR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2748, 'JOR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2761, 'KAZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2762, 'KAZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2763, 'KAZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2764, 'KAZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2765, 'KAZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2766, 'KAZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2767, 'KAZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2768, 'KAZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2769, 'KAZ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2770, 'KAZ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2771, 'KAZ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2772, 'KAZ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2785, 'KEN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2786, 'KEN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2787, 'KEN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2788, 'KEN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2789, 'KEN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2790, 'KEN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2791, 'KEN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2792, 'KEN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2793, 'KEN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2794, 'KEN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2795, 'KEN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2796, 'KEN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2809, 'KIR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2810, 'KIR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2811, 'KIR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2812, 'KIR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2813, 'KIR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2814, 'KIR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2815, 'KIR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2816, 'KIR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2817, 'KIR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2818, 'KIR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2819, 'KIR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2820, 'KIR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2833, 'PRK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2834, 'PRK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2835, 'PRK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2836, 'PRK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2837, 'PRK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2838, 'PRK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2839, 'PRK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2840, 'PRK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2841, 'PRK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2842, 'PRK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2843, 'PRK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2844, 'PRK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2857, 'KOR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2858, 'KOR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2859, 'KOR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2860, 'KOR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2861, 'KOR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2862, 'KOR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2863, 'KOR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2864, 'KOR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2865, 'KOR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2866, 'KOR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2867, 'KOR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2868, 'KOR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2881, 'KWT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2882, 'KWT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2883, 'KWT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2884, 'KWT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2885, 'KWT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2886, 'KWT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2887, 'KWT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2888, 'KWT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2889, 'KWT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2890, 'KWT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2891, 'KWT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2892, 'KWT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2905, 'KGZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2906, 'KGZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2907, 'KGZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2908, 'KGZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2909, 'KGZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2910, 'KGZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2911, 'KGZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2912, 'KGZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2913, 'KGZ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2914, 'KGZ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2915, 'KGZ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2916, 'KGZ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2929, 'LAO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2930, 'LAO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2931, 'LAO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2932, 'LAO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2933, 'LAO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2934, 'LAO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2935, 'LAO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2936, 'LAO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2937, 'LAO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2938, 'LAO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2939, 'LAO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2940, 'LAO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2953, 'LVA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2954, 'LVA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2955, 'LVA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2956, 'LVA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2957, 'LVA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2958, 'LVA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2959, 'LVA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2960, 'LVA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2961, 'LVA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2962, 'LVA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2963, 'LVA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2964, 'LVA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2969, 'LVA', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2970, 'LVA', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2971, 'LVA', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2972, 'LVA', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2985, 'LBN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2986, 'LBN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2987, 'LBN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2988, 'LBN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2989, 'LBN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2990, 'LBN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2991, 'LBN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2992, 'LBN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2993, 'LBN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2994, 'LBN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2995, 'LBN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (2996, 'LBN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3009, 'LSO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3010, 'LSO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3011, 'LSO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3012, 'LSO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3013, 'LSO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3014, 'LSO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3015, 'LSO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3016, 'LSO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3017, 'LSO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3018, 'LSO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3019, 'LSO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3020, 'LSO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3033, 'LBR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3034, 'LBR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3035, 'LBR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3036, 'LBR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3037, 'LBR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3038, 'LBR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3039, 'LBR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3040, 'LBR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3041, 'LBR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3042, 'LBR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3043, 'LBR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3044, 'LBR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3057, 'LBY', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3058, 'LBY', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3059, 'LBY', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3060, 'LBY', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3061, 'LBY', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3062, 'LBY', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3063, 'LBY', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3064, 'LBY', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3065, 'LBY', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3066, 'LBY', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3067, 'LBY', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3068, 'LBY', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3081, 'LIE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3082, 'LIE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3083, 'LIE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3084, 'LIE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3085, 'LIE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3086, 'LIE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3087, 'LIE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3088, 'LIE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3089, 'LIE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3090, 'LIE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3091, 'LIE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3092, 'LIE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3105, 'LTU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3106, 'LTU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3107, 'LTU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3108, 'LTU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3109, 'LTU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3110, 'LTU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3111, 'LTU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3112, 'LTU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3113, 'LTU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3114, 'LTU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3115, 'LTU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3116, 'LTU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3121, 'LTU', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3122, 'LTU', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3123, 'LTU', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3124, 'LTU', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3137, 'LUX', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3138, 'LUX', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3139, 'LUX', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3140, 'LUX', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3141, 'LUX', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3142, 'LUX', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3143, 'LUX', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3144, 'LUX', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3145, 'LUX', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3146, 'LUX', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3147, 'LUX', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3148, 'LUX', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3153, 'LUX', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3154, 'LUX', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3155, 'LUX', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3156, 'LUX', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3169, 'MAC', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3170, 'MAC', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3171, 'MAC', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3172, 'MAC', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3173, 'MAC', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3174, 'MAC', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3175, 'MAC', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3176, 'MAC', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3177, 'MAC', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3178, 'MAC', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3179, 'MAC', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3180, 'MAC', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3193, 'MKD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3194, 'MKD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3195, 'MKD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3196, 'MKD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3197, 'MKD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3198, 'MKD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3199, 'MKD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3200, 'MKD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3201, 'MKD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3202, 'MKD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3203, 'MKD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3204, 'MKD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3217, 'MDG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3218, 'MDG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3219, 'MDG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3220, 'MDG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3221, 'MDG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3222, 'MDG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3223, 'MDG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3224, 'MDG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3225, 'MDG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3226, 'MDG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3227, 'MDG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3228, 'MDG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3241, 'MWI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3242, 'MWI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3243, 'MWI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3244, 'MWI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3245, 'MWI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3246, 'MWI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3247, 'MWI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3248, 'MWI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3249, 'MWI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3250, 'MWI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3251, 'MWI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3252, 'MWI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3265, 'MYS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3266, 'MYS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3267, 'MYS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3268, 'MYS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3269, 'MYS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3270, 'MYS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3271, 'MYS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3272, 'MYS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3273, 'MYS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3274, 'MYS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3275, 'MYS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3276, 'MYS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3289, 'MDV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3290, 'MDV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3291, 'MDV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3292, 'MDV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3293, 'MDV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3294, 'MDV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3295, 'MDV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3296, 'MDV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3297, 'MDV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3298, 'MDV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3299, 'MDV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3300, 'MDV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3313, 'MLI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3314, 'MLI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3315, 'MLI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3316, 'MLI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3317, 'MLI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3318, 'MLI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3319, 'MLI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3320, 'MLI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3321, 'MLI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3322, 'MLI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3323, 'MLI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3324, 'MLI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3337, 'MLT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3338, 'MLT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3339, 'MLT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3340, 'MLT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3341, 'MLT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3342, 'MLT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3343, 'MLT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3344, 'MLT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3345, 'MLT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3346, 'MLT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3347, 'MLT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3348, 'MLT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3353, 'MLT', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3354, 'MLT', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3355, 'MLT', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3356, 'MLT', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3369, 'MHL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3370, 'MHL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3371, 'MHL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3372, 'MHL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3373, 'MHL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3374, 'MHL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3375, 'MHL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3376, 'MHL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3377, 'MHL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3378, 'MHL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3379, 'MHL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3380, 'MHL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3393, 'MTQ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3394, 'MTQ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3395, 'MTQ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3396, 'MTQ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3397, 'MTQ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3398, 'MTQ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3399, 'MTQ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3400, 'MTQ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3401, 'MTQ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3402, 'MTQ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3403, 'MTQ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3404, 'MTQ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3417, 'MRT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3418, 'MRT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3419, 'MRT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3420, 'MRT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3421, 'MRT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3422, 'MRT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3423, 'MRT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3424, 'MRT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3425, 'MRT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3426, 'MRT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3427, 'MRT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3428, 'MRT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3441, 'MUS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3442, 'MUS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3443, 'MUS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3444, 'MUS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3445, 'MUS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3446, 'MUS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3447, 'MUS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3448, 'MUS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3449, 'MUS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3450, 'MUS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3451, 'MUS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3452, 'MUS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3465, 'MYT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3466, 'MYT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3467, 'MYT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3468, 'MYT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3469, 'MYT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3470, 'MYT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3471, 'MYT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3472, 'MYT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3473, 'MYT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3474, 'MYT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3475, 'MYT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3476, 'MYT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3489, 'MEX', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3490, 'MEX', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3491, 'MEX', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3492, 'MEX', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3493, 'MEX', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3494, 'MEX', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3495, 'MEX', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3496, 'MEX', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3505, 'MEX', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3506, 'MEX', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3507, 'MEX', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3508, 'MEX', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3521, 'FSM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3522, 'FSM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3523, 'FSM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3524, 'FSM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3525, 'FSM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3526, 'FSM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3527, 'FSM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3528, 'FSM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3529, 'FSM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3530, 'FSM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3531, 'FSM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3532, 'FSM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3545, 'MDA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3546, 'MDA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3547, 'MDA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3548, 'MDA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3549, 'MDA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3550, 'MDA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3551, 'MDA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3552, 'MDA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3553, 'MDA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3554, 'MDA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3555, 'MDA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3556, 'MDA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3569, 'MCO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3570, 'MCO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3571, 'MCO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3572, 'MCO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3573, 'MCO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3574, 'MCO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3575, 'MCO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3576, 'MCO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3577, 'MCO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3578, 'MCO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3579, 'MCO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3580, 'MCO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3593, 'MNG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3594, 'MNG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3595, 'MNG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3596, 'MNG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3597, 'MNG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3598, 'MNG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3599, 'MNG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3600, 'MNG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3601, 'MNG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3602, 'MNG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3603, 'MNG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3604, 'MNG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3617, 'MNE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3618, 'MNE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3619, 'MNE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3620, 'MNE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3621, 'MNE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3622, 'MNE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3623, 'MNE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3624, 'MNE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3625, 'MNE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3626, 'MNE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3627, 'MNE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3628, 'MNE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3641, 'MSR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3642, 'MSR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3643, 'MSR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3644, 'MSR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3645, 'MSR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3646, 'MSR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3647, 'MSR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3648, 'MSR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3649, 'MSR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3650, 'MSR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3651, 'MSR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3652, 'MSR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3665, 'MAR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3666, 'MAR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3667, 'MAR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3668, 'MAR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3669, 'MAR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3670, 'MAR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3671, 'MAR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3672, 'MAR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3673, 'MAR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3674, 'MAR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3675, 'MAR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3676, 'MAR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3689, 'MOZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3690, 'MOZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3691, 'MOZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3692, 'MOZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3693, 'MOZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3694, 'MOZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3695, 'MOZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3696, 'MOZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3697, 'MOZ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3698, 'MOZ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3699, 'MOZ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3700, 'MOZ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3713, 'MMR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3714, 'MMR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3715, 'MMR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3716, 'MMR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3717, 'MMR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3718, 'MMR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3719, 'MMR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3720, 'MMR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3721, 'MMR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3722, 'MMR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3723, 'MMR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3724, 'MMR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3737, 'NAM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3738, 'NAM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3739, 'NAM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3740, 'NAM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3741, 'NAM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3742, 'NAM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3743, 'NAM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3744, 'NAM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3745, 'NAM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3746, 'NAM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3747, 'NAM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3748, 'NAM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3761, 'NRU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3762, 'NRU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3763, 'NRU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3764, 'NRU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3765, 'NRU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3766, 'NRU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3767, 'NRU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3768, 'NRU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3769, 'NRU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3770, 'NRU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3771, 'NRU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3772, 'NRU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3785, 'NPL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3786, 'NPL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3787, 'NPL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3788, 'NPL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3789, 'NPL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3790, 'NPL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3791, 'NPL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3792, 'NPL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3793, 'NPL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3794, 'NPL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3795, 'NPL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3796, 'NPL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3809, 'NLD', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3810, 'NLD', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3811, 'NLD', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3812, 'NLD', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3813, 'NLD', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3814, 'NLD', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3815, 'NLD', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3816, 'NLD', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3817, 'NLD', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3818, 'NLD', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3819, 'NLD', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3820, 'NLD', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3825, 'NLD', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3826, 'NLD', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3827, 'NLD', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3828, 'NLD', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);



  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3849, 'ANT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3850, 'ANT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3851, 'ANT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3852, 'ANT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3853, 'ANT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3854, 'ANT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3855, 'ANT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3856, 'ANT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3857, 'ANT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3858, 'ANT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3859, 'ANT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3860, 'ANT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3873, 'NCL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3874, 'NCL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3875, 'NCL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3876, 'NCL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3877, 'NCL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3878, 'NCL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3879, 'NCL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3880, 'NCL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3881, 'NCL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3882, 'NCL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3883, 'NCL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3884, 'NCL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3897, 'NZL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3898, 'NZL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3899, 'NZL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3900, 'NZL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3901, 'NZL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3902, 'NZL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3903, 'NZL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3904, 'NZL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3905, 'NZL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3906, 'NZL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3907, 'NZL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3908, 'NZL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3921, 'NIC', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3922, 'NIC', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3923, 'NIC', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3924, 'NIC', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3925, 'NIC', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3926, 'NIC', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3927, 'NIC', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3928, 'NIC', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3929, 'NIC', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3930, 'NIC', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3931, 'NIC', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3932, 'NIC', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3945, 'NER', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3946, 'NER', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3947, 'NER', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3948, 'NER', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3949, 'NER', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3950, 'NER', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3951, 'NER', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3952, 'NER', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3953, 'NER', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3954, 'NER', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3955, 'NER', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3956, 'NER', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3969, 'NGA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3970, 'NGA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3971, 'NGA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3972, 'NGA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3973, 'NGA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3974, 'NGA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3975, 'NGA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3976, 'NGA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3977, 'NGA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3978, 'NGA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3979, 'NGA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3980, 'NGA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3993, 'NIU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3994, 'NIU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3995, 'NIU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3996, 'NIU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3997, 'NIU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3998, 'NIU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (3999, 'NIU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4000, 'NIU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4001, 'NIU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4002, 'NIU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4003, 'NIU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4004, 'NIU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4017, 'NFK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4018, 'NFK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4019, 'NFK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4020, 'NFK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4021, 'NFK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4022, 'NFK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4023, 'NFK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4024, 'NFK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4025, 'NFK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4026, 'NFK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4027, 'NFK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4028, 'NFK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4041, 'MNP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4042, 'MNP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4043, 'MNP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4044, 'MNP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4045, 'MNP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4046, 'MNP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4047, 'MNP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4048, 'MNP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4049, 'MNP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4050, 'MNP', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4051, 'MNP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4052, 'MNP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4065, 'NOR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4066, 'NOR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4067, 'NOR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4068, 'NOR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4069, 'NOR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4070, 'NOR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4071, 'NOR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4072, 'NOR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4073, 'NOR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4074, 'NOR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4075, 'NOR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4076, 'NOR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4089, 'OMN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4090, 'OMN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4091, 'OMN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4092, 'OMN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4093, 'OMN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4094, 'OMN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4095, 'OMN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4096, 'OMN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4097, 'OMN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4098, 'OMN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4099, 'OMN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4100, 'OMN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4113, 'PAK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4114, 'PAK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4115, 'PAK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4116, 'PAK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4117, 'PAK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4118, 'PAK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4119, 'PAK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4120, 'PAK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4121, 'PAK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4122, 'PAK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4123, 'PAK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4124, 'PAK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4137, 'PLW', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4138, 'PLW', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4139, 'PLW', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4140, 'PLW', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4141, 'PLW', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4142, 'PLW', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4143, 'PLW', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4144, 'PLW', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4145, 'PLW', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4146, 'PLW', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4147, 'PLW', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4148, 'PLW', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4161, 'PSE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4162, 'PSE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4163, 'PSE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4164, 'PSE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4165, 'PSE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4166, 'PSE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4167, 'PSE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4168, 'PSE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4169, 'PSE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4170, 'PSE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4171, 'PSE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4172, 'PSE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4185, 'PAN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4186, 'PAN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4187, 'PAN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4188, 'PAN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4189, 'PAN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4190, 'PAN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4191, 'PAN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4192, 'PAN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4193, 'PAN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4194, 'PAN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4195, 'PAN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4196, 'PAN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4209, 'PNG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4210, 'PNG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4211, 'PNG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4212, 'PNG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4213, 'PNG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4214, 'PNG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4215, 'PNG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4216, 'PNG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4217, 'PNG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4218, 'PNG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4219, 'PNG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4220, 'PNG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4233, 'PRY', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4234, 'PRY', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4235, 'PRY', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4236, 'PRY', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4237, 'PRY', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4238, 'PRY', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4239, 'PRY', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4240, 'PRY', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4241, 'PRY', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4242, 'PRY', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4243, 'PRY', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4244, 'PRY', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4257, 'PER', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4258, 'PER', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4259, 'PER', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4260, 'PER', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4261, 'PER', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4262, 'PER', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4263, 'PER', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4264, 'PER', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4265, 'PER', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4266, 'PER', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4267, 'PER', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4268, 'PER', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4281, 'PHL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4282, 'PHL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4283, 'PHL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4284, 'PHL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4285, 'PHL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4286, 'PHL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4287, 'PHL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4288, 'PHL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4289, 'PHL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4290, 'PHL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4291, 'PHL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4292, 'PHL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4305, 'PCN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4306, 'PCN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4307, 'PCN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4308, 'PCN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4309, 'PCN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4310, 'PCN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4311, 'PCN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4312, 'PCN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4313, 'PCN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4314, 'PCN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4315, 'PCN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4316, 'PCN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4329, 'POL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4330, 'POL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4331, 'POL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4332, 'POL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4333, 'POL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4334, 'POL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4335, 'POL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4336, 'POL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4337, 'POL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4338, 'POL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4339, 'POL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4340, 'POL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4345, 'POL', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4346, 'POL', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4347, 'POL', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4348, 'POL', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4361, 'PRT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4362, 'PRT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4363, 'PRT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4364, 'PRT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4365, 'PRT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4366, 'PRT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4367, 'PRT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4368, 'PRT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4369, 'PRT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4370, 'PRT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4371, 'PRT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4372, 'PRT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4377, 'PRT', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4378, 'PRT', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4379, 'PRT', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4380, 'PRT', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4393, 'PRI', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4394, 'PRI', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4395, 'PRI', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4396, 'PRI', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4397, 'PRI', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4398, 'PRI', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4399, 'PRI', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4400, 'PRI', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4401, 'PRI', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4402, 'PRI', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4403, 'PRI', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4404, 'PRI', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4417, 'QAT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4418, 'QAT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4419, 'QAT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4420, 'QAT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4421, 'QAT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4422, 'QAT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4423, 'QAT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4424, 'QAT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4425, 'QAT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4426, 'QAT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4427, 'QAT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4428, 'QAT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4441, 'REU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4442, 'REU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4443, 'REU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4444, 'REU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4445, 'REU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4446, 'REU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4447, 'REU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4448, 'REU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4449, 'REU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4450, 'REU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4451, 'REU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4452, 'REU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4465, 'ROU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4466, 'ROU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4467, 'ROU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4468, 'ROU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4469, 'ROU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4470, 'ROU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4471, 'ROU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4472, 'ROU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4473, 'ROU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4474, 'ROU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4475, 'ROU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4476, 'ROU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4481, 'ROU', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4482, 'ROU', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4483, 'ROU', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4484, 'ROU', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4497, 'RUS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4498, 'RUS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4499, 'RUS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4500, 'RUS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4501, 'RUS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4502, 'RUS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4503, 'RUS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4504, 'RUS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4505, 'RUS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4506, 'RUS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4507, 'RUS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4508, 'RUS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);






  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4553, 'RWA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4554, 'RWA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4555, 'RWA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4556, 'RWA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4557, 'RWA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4558, 'RWA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4559, 'RWA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4560, 'RWA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4561, 'RWA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4562, 'RWA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4563, 'RWA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4564, 'RWA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4577, 'SHN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4578, 'SHN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4579, 'SHN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4580, 'SHN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4581, 'SHN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4582, 'SHN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4583, 'SHN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4584, 'SHN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4585, 'SHN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4586, 'SHN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4587, 'SHN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4588, 'SHN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4601, 'KNA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4602, 'KNA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4603, 'KNA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4604, 'KNA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4605, 'KNA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4606, 'KNA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4607, 'KNA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4608, 'KNA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4609, 'KNA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4610, 'KNA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4611, 'KNA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4612, 'KNA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4625, 'LCA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4626, 'LCA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4627, 'LCA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4628, 'LCA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4629, 'LCA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4630, 'LCA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4631, 'LCA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4632, 'LCA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4633, 'LCA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4634, 'LCA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4635, 'LCA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4636, 'LCA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4649, 'SPM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4650, 'SPM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4651, 'SPM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4652, 'SPM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4653, 'SPM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4654, 'SPM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4655, 'SPM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4656, 'SPM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4657, 'SPM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4658, 'SPM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4659, 'SPM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4660, 'SPM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4673, 'VCT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4674, 'VCT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4675, 'VCT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4676, 'VCT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4677, 'VCT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4678, 'VCT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4679, 'VCT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4680, 'VCT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4681, 'VCT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4682, 'VCT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4683, 'VCT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4684, 'VCT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4697, 'WSM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4698, 'WSM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4699, 'WSM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4700, 'WSM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4701, 'WSM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4702, 'WSM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4703, 'WSM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4704, 'WSM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4705, 'WSM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4706, 'WSM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4707, 'WSM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4708, 'WSM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4721, 'SMR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4722, 'SMR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4723, 'SMR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4724, 'SMR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4725, 'SMR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4726, 'SMR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4727, 'SMR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4728, 'SMR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4729, 'SMR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4730, 'SMR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4731, 'SMR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4732, 'SMR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4745, 'STP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4746, 'STP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4747, 'STP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4748, 'STP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4749, 'STP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4750, 'STP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4751, 'STP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4752, 'STP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4753, 'STP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4754, 'STP', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4755, 'STP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4756, 'STP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4769, 'SAU', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4770, 'SAU', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4771, 'SAU', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4772, 'SAU', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4773, 'SAU', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4774, 'SAU', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4775, 'SAU', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4776, 'SAU', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4777, 'SAU', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4778, 'SAU', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4779, 'SAU', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4780, 'SAU', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4793, 'SEN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4794, 'SEN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4795, 'SEN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4796, 'SEN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4797, 'SEN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4798, 'SEN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4799, 'SEN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4800, 'SEN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4801, 'SEN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4802, 'SEN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4803, 'SEN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4804, 'SEN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4817, 'SCG', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4818, 'SCG', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4819, 'SCG', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4820, 'SCG', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4821, 'SCG', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4822, 'SCG', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4823, 'SCG', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4824, 'SCG', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4825, 'SCG', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4826, 'SCG', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4827, 'SCG', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4828, 'SCG', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4841, 'SYC', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4842, 'SYC', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4843, 'SYC', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4844, 'SYC', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4845, 'SYC', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4846, 'SYC', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4847, 'SYC', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4848, 'SYC', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4849, 'SYC', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4850, 'SYC', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4851, 'SYC', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4852, 'SYC', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4865, 'SLE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4866, 'SLE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4867, 'SLE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4868, 'SLE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4869, 'SLE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4870, 'SLE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4871, 'SLE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4872, 'SLE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4873, 'SLE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4874, 'SLE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4875, 'SLE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4876, 'SLE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4889, 'SGP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4890, 'SGP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4891, 'SGP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4892, 'SGP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4893, 'SGP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4894, 'SGP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4895, 'SGP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4896, 'SGP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4897, 'SGP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4898, 'SGP', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4899, 'SGP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4900, 'SGP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4913, 'SVK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4914, 'SVK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4915, 'SVK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4916, 'SVK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4917, 'SVK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4918, 'SVK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4919, 'SVK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4920, 'SVK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4921, 'SVK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4922, 'SVK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4923, 'SVK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4924, 'SVK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4929, 'SVK', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4930, 'SVK', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4931, 'SVK', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4932, 'SVK', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4945, 'SVN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4946, 'SVN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4947, 'SVN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4948, 'SVN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4949, 'SVN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4950, 'SVN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4951, 'SVN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4952, 'SVN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4953, 'SVN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4954, 'SVN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4955, 'SVN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4956, 'SVN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4961, 'SVN', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4962, 'SVN', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4963, 'SVN', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4964, 'SVN', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4977, 'SLB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4978, 'SLB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4979, 'SLB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4980, 'SLB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4981, 'SLB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4982, 'SLB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4983, 'SLB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4984, 'SLB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4985, 'SLB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4986, 'SLB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4987, 'SLB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (4988, 'SLB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5001, 'SOM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5002, 'SOM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5003, 'SOM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5004, 'SOM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5005, 'SOM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5006, 'SOM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5007, 'SOM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5008, 'SOM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5009, 'SOM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5010, 'SOM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5011, 'SOM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5012, 'SOM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5025, 'ZAF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5026, 'ZAF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5027, 'ZAF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5028, 'ZAF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5029, 'ZAF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5030, 'ZAF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5031, 'ZAF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5032, 'ZAF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5033, 'ZAF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5034, 'ZAF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5035, 'ZAF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5036, 'ZAF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5049, 'SGS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5050, 'SGS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5051, 'SGS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5052, 'SGS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5053, 'SGS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5054, 'SGS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5055, 'SGS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5056, 'SGS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5057, 'SGS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5058, 'SGS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5059, 'SGS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5060, 'SGS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5073, 'ESP', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5074, 'ESP', 9, 6, 1, 9, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5075, 'ESP', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5076, 'ESP', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5081, 'ESP', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5082, 'ESP', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5083, 'ESP', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5084, 'ESP', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5085, 'ESP', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5086, 'ESP', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5087, 'ESP', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5088, 'ESP', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5089, 'ESP', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,2,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5090, 'ESP', 5, 7, 2, 12, 'localhost',FALSE,2,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5091, 'ESP', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,2,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5092, 'ESP', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,2,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5105, 'LKA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5106, 'LKA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5107, 'LKA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5108, 'LKA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5109, 'LKA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5110, 'LKA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5111, 'LKA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5112, 'LKA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5113, 'LKA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5114, 'LKA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5115, 'LKA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5116, 'LKA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5129, 'SDN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5130, 'SDN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5131, 'SDN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5132, 'SDN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5133, 'SDN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5134, 'SDN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5135, 'SDN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5136, 'SDN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5137, 'SDN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5138, 'SDN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5139, 'SDN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5140, 'SDN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5153, 'SUR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5154, 'SUR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5155, 'SUR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5156, 'SUR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5157, 'SUR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5158, 'SUR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5159, 'SUR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5160, 'SUR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5161, 'SUR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5162, 'SUR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5163, 'SUR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5164, 'SUR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5177, 'SJM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5178, 'SJM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5179, 'SJM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5180, 'SJM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5181, 'SJM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5182, 'SJM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5183, 'SJM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5184, 'SJM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5185, 'SJM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5186, 'SJM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5187, 'SJM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5188, 'SJM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5201, 'SWZ', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5202, 'SWZ', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5203, 'SWZ', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5204, 'SWZ', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5205, 'SWZ', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5206, 'SWZ', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5207, 'SWZ', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5208, 'SWZ', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5209, 'SWZ', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5210, 'SWZ', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5211, 'SWZ', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5212, 'SWZ', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5225, 'SWE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5226, 'SWE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5227, 'SWE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5228, 'SWE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5229, 'SWE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5230, 'SWE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5231, 'SWE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5232, 'SWE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5233, 'SWE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5234, 'SWE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5235, 'SWE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5236, 'SWE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5241, 'SWE', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5242, 'SWE', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5243, 'SWE', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5244, 'SWE', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5257, 'CHE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5258, 'CHE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5259, 'CHE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5260, 'CHE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5261, 'CHE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5262, 'CHE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5263, 'CHE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5264, 'CHE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5265, 'CHE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5266, 'CHE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5267, 'CHE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5268, 'CHE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5273, 'CHE', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5274, 'CHE', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5275, 'CHE', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5276, 'CHE', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5289, 'SYR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5290, 'SYR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5291, 'SYR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5292, 'SYR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5293, 'SYR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5294, 'SYR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5295, 'SYR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5296, 'SYR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5297, 'SYR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5298, 'SYR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5299, 'SYR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5300, 'SYR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5313, 'TWN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5314, 'TWN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5315, 'TWN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5316, 'TWN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5317, 'TWN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5318, 'TWN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5319, 'TWN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5320, 'TWN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5321, 'TWN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5322, 'TWN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5323, 'TWN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5324, 'TWN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5337, 'TJK', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5338, 'TJK', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5339, 'TJK', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5340, 'TJK', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5341, 'TJK', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5342, 'TJK', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5343, 'TJK', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5344, 'TJK', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5345, 'TJK', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5346, 'TJK', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5347, 'TJK', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5348, 'TJK', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5361, 'TZA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5362, 'TZA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5363, 'TZA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5364, 'TZA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5365, 'TZA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5366, 'TZA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5367, 'TZA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5368, 'TZA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5369, 'TZA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5370, 'TZA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5371, 'TZA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5372, 'TZA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5385, 'THA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5386, 'THA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5387, 'THA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5388, 'THA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5389, 'THA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5390, 'THA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5391, 'THA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5392, 'THA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5393, 'THA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5394, 'THA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5395, 'THA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5396, 'THA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5409, 'TLS', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5410, 'TLS', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5411, 'TLS', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5412, 'TLS', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5413, 'TLS', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5414, 'TLS', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5415, 'TLS', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5416, 'TLS', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5417, 'TLS', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5418, 'TLS', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5419, 'TLS', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5420, 'TLS', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5433, 'TGO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5434, 'TGO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5435, 'TGO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5436, 'TGO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5437, 'TGO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5438, 'TGO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5439, 'TGO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5440, 'TGO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5441, 'TGO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5442, 'TGO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5443, 'TGO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5444, 'TGO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5457, 'TKL', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5458, 'TKL', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5459, 'TKL', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5460, 'TKL', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5461, 'TKL', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5462, 'TKL', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5463, 'TKL', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5464, 'TKL', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5465, 'TKL', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5466, 'TKL', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5467, 'TKL', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5468, 'TKL', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5481, 'TON', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5482, 'TON', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5483, 'TON', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5484, 'TON', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5485, 'TON', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5486, 'TON', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5487, 'TON', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5488, 'TON', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5489, 'TON', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5490, 'TON', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5491, 'TON', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5492, 'TON', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5505, 'TTO', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5506, 'TTO', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5507, 'TTO', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5508, 'TTO', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5509, 'TTO', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5510, 'TTO', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5511, 'TTO', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5512, 'TTO', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5513, 'TTO', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5514, 'TTO', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5515, 'TTO', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5516, 'TTO', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5529, 'TUN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5530, 'TUN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5531, 'TUN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5532, 'TUN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5533, 'TUN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5534, 'TUN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5535, 'TUN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5536, 'TUN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5537, 'TUN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5538, 'TUN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5539, 'TUN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5540, 'TUN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5553, 'TUR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5554, 'TUR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5555, 'TUR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5556, 'TUR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5557, 'TUR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5558, 'TUR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5559, 'TUR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5560, 'TUR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5569, 'TKM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5570, 'TKM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5571, 'TKM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5572, 'TKM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5573, 'TKM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5574, 'TKM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5575, 'TKM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5576, 'TKM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5577, 'TKM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5578, 'TKM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5579, 'TKM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5580, 'TKM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5593, 'TCA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5594, 'TCA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5595, 'TCA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5596, 'TCA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5597, 'TCA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5598, 'TCA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5599, 'TCA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5600, 'TCA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5601, 'TCA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5602, 'TCA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5603, 'TCA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5604, 'TCA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5617, 'TUV', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5618, 'TUV', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5619, 'TUV', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5620, 'TUV', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5621, 'TUV', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5622, 'TUV', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5623, 'TUV', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5624, 'TUV', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5625, 'TUV', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5626, 'TUV', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5627, 'TUV', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5628, 'TUV', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5641, 'UGA', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5642, 'UGA', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5643, 'UGA', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5644, 'UGA', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5645, 'UGA', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5646, 'UGA', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5647, 'UGA', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5648, 'UGA', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5649, 'UGA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5650, 'UGA', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5651, 'UGA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5652, 'UGA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5665, 'UKR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5666, 'UKR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5667, 'UKR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5668, 'UKR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5669, 'UKR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5670, 'UKR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5671, 'UKR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5672, 'UKR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5673, 'UKR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5674, 'UKR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5675, 'UKR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5676, 'UKR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5681, 'UKR', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5682, 'UKR', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5683, 'UKR', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5684, 'UKR', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5697, 'ARE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5698, 'ARE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5699, 'ARE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5700, 'ARE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5701, 'ARE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5702, 'ARE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5703, 'ARE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5704, 'ARE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5705, 'ARE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5706, 'ARE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5707, 'ARE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5708, 'ARE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5721, 'GBR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5722, 'GBR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5723, 'GBR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5724, 'GBR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5725, 'GBR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5726, 'GBR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5727, 'GBR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5728, 'GBR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5729, 'GBR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5730, 'GBR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5731, 'GBR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5732, 'GBR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5737, 'GBR', 9, 6, 1, 3, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5738, 'GBR', 9, 6, 1, 9, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5739, 'GBR', 9, 6, 1, 15, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5740, 'GBR', 9, 6, 1, 21, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5753, 'USA', 4, 2, 1, 4, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5754, 'USA', 4, 2, 1, 10, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5755, 'USA', 4, 2, 1, 16, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5756, 'USA', 4, 2, 1, 22, 'premium.abaenglish.com',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5757, 'USA', 4, 6, 1, 1, 'payments-dev.aba.land',FALSE,3,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5758, 'USA', 4, 6, 1, 7, 'localhost',FALSE,3,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5759, 'USA', 4, 6, 1, 13, 'payments-qa.aba.land',FALSE,3,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5760, 'USA', 4, 6, 1, 19, 'premium.abaenglish.com',FALSE,3,FALSE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5769, 'USA', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5770, 'USA', 5, 7, 2, 12, 'localhost',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5771, 'USA', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,4,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5772, 'USA', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,4,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5785, 'URY', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5786, 'URY', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5787, 'URY', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5788, 'URY', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5789, 'URY', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5790, 'URY', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5791, 'URY', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5792, 'URY', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5793, 'URY', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5794, 'URY', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5795, 'URY', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5796, 'URY', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5809, 'UZB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5810, 'UZB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5811, 'UZB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5812, 'UZB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5813, 'UZB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5814, 'UZB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5815, 'UZB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5816, 'UZB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5817, 'UZB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5818, 'UZB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5819, 'UZB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5820, 'UZB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5833, 'VUT', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5834, 'VUT', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5835, 'VUT', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5836, 'VUT', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5837, 'VUT', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5838, 'VUT', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5839, 'VUT', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5840, 'VUT', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5841, 'VUT', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5842, 'VUT', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5843, 'VUT', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5844, 'VUT', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5857, 'VEN', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5858, 'VEN', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5859, 'VEN', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5860, 'VEN', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5861, 'VEN', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5862, 'VEN', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5863, 'VEN', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5864, 'VEN', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5865, 'VEN', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5866, 'VEN', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5867, 'VEN', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5868, 'VEN', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5881, 'VNM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5882, 'VNM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5883, 'VNM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5884, 'VNM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5885, 'VNM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5886, 'VNM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5887, 'VNM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5888, 'VNM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5889, 'VNM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5890, 'VNM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5891, 'VNM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5892, 'VNM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5905, 'VGB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5906, 'VGB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5907, 'VGB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5908, 'VGB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5909, 'VGB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5910, 'VGB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5911, 'VGB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5912, 'VGB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5913, 'VGB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5914, 'VGB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5915, 'VGB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5916, 'VGB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5929, 'VIR', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5930, 'VIR', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5931, 'VIR', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5932, 'VIR', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5933, 'VIR', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5934, 'VIR', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5935, 'VIR', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5936, 'VIR', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5937, 'VIR', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5938, 'VIR', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5939, 'VIR', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5940, 'VIR', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5953, 'WLF', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5954, 'WLF', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5955, 'WLF', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5956, 'WLF', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5957, 'WLF', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5958, 'WLF', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5959, 'WLF', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5960, 'WLF', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5961, 'WLF', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5962, 'WLF', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5963, 'WLF', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5964, 'WLF', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5977, 'ESH', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5978, 'ESH', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5979, 'ESH', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5980, 'ESH', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5981, 'ESH', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5982, 'ESH', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5983, 'ESH', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5984, 'ESH', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5985, 'ESH', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5986, 'ESH', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5987, 'ESH', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (5988, 'ESH', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6001, 'YEM', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6002, 'YEM', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6003, 'YEM', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6004, 'YEM', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6005, 'YEM', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6006, 'YEM', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6007, 'YEM', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6008, 'YEM', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6009, 'YEM', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6010, 'YEM', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6011, 'YEM', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6012, 'YEM', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6025, 'ZMB', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6026, 'ZMB', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6027, 'ZMB', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6028, 'ZMB', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6029, 'ZMB', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6030, 'ZMB', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6031, 'ZMB', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6032, 'ZMB', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6033, 'ZMB', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6034, 'ZMB', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6035, 'ZMB', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6036, 'ZMB', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);


  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6049, 'ZWE', 4, 2, 1, 4, 'payments-dev.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6050, 'ZWE', 4, 2, 1, 10, 'localhost',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6051, 'ZWE', 4, 2, 1, 16, 'payments-qa.aba.land',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6052, 'ZWE', 4, 2, 1, 22, 'premium.abaenglish.com',TRUE,1,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6053, 'ZWE', 4, 6, 1, 1, 'payments-dev.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6054, 'ZWE', 4, 6, 1, 7, 'localhost',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6055, 'ZWE', 4, 6, 1, 13, 'payments-qa.aba.land',TRUE,1,FALSE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6056, 'ZWE', 4, 6, 1, 19, 'premium.abaenglish.com',TRUE,1,FALSE);

  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6057, 'ZWE', 5, 7, 2, 6, 'payments-dev.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6058, 'ZWE', 5, 7, 2, 12, 'localhost',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6059, 'ZWE', 5, 7, 2, 18, 'payments-qa.aba.land',FALSE,3,TRUE);
  INSERT INTO payment_form (id, country_iso3, method_id, gateway_id, provider_id, hostedpages_id, environmenturl, defaultoption, payment_order, active) VALUES (6060, 'ZWE', 5, 7, 2, 24, 'premium.abaenglish.com',FALSE,3,TRUE);

